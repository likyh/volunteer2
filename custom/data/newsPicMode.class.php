<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 14-4-30
 * Time: 下午6:57
 */
/**
 * Class newsPicMode
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class newsPicMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    public function getList($rows=20,$offset=0){
        //TODO 这里是实验的，返回的不是一个数组，而是一个SqlDBStatement
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select `id`, `title`, `content`, `author`,`picurl`,`date` from `text_news_pic`
              where `enable`=1 order by `create_time` DESC limit {$offset},{$rows}";

        $statement=$this->db->query($sql);
        import("Lib.String.TextFilter");
        $statement->setDealMethod(function($data){
            $data['content']=TextFilter::htmlCut($data['content'],0,80);
            return $data;
        });
        $result=$statement->getArray();
        return $result;
    }

    public function getRowsTotal(){
        $countSql="select count(1) from text_news_pic where `enable`=1";
        return $this->db->getValue($countSql);
    }

    public function content($id){
        $id=(int)$id;
        $sql="select `id`,`title`, `author`, `date`, `content`, `picurl` from `text_news_pic` where `id`=$id ";
        $result=$this->db->getOne($sql);
        //获取上一篇下一篇  $result['preId'],$result['nextId']
        $total=$this->db->getValue("select count(*) from `text_news_pic` where `enable`=1");
        $getAllDesc="SELECT `id`,`title` FROM `text_news_pic` WHERE `enable`=1  order by `create_time` DESC";
        $re=$this->db->getAll($getAllDesc);
        for($i=0;$i<$total;$i++){
            if($re[$i]['id']==$id){
                if($i==0){
                    $result['preId']=$result['preTitle']=NULL;
                }else{
                    $result['preId']=$re[$i-1]['id'];
                    $result['preTitle']=$re[$i-1]['title'];
                }
                if($i==$total-1){
                    $result['nextId']=$result['nextTitle']=NULL;
                }else{
                    $result['nextId']=$re[$i+1]['id'];
                    $result['nextTitle']=$re[$i+1]['title'];
                }
            }
        }
        return $result;
    }

    public function createSubmit($title,$author,$date,$content,$picurl,$typeid){
        $data['title']=empty($title)?'':$title;
        //先检查是否有重名新闻
        $checkIfExist="select * from text_news_pic where `title`='{$data['title']}'";
        $checkResult=$this->db->getExist($checkIfExist);
        if($checkResult) return false;
        $data['typeid']=empty($typeid)?'':$typeid;
        $data['date']=empty($date)?'':$date;
        $data['author']=empty($author)?'':$author;
        $data['content']=empty($content)?'':$content;
        $data['picurl']=empty($picurl)?'':$picurl;
        if($this->db->insert('text_news_pic',$data)){
            return true;
        }else return false;
    }

    public function modifyShow($id){
        $id=(int)$id;
        $sql="select `text_news_pic`.`id` as `id`, `title`, `author`, `date`, `content`, `picurl`,`typeid`,`nickname` as `type` from `text_news_pic`,`admin_user` where `text_news_pic`.`id`=$id and `text_news_pic`.`typeid`=`admin_user`.`admin_user_id`";
        $result=$this->db->getOne($sql);
        return $result;
    }

    public function modifySubmit($id,$title,$author,$date,$content,$picurl,$typeid){
        $id=(int)$id;
        $data['title']=empty($title)?'':htmlentities($title,ENT_NOQUOTES,"utf-8");
        $data['typeid']=empty($typeid)?'':$typeid;
        $data['date']=empty($date)?'':htmlentities($date,ENT_NOQUOTES,"utf-8");
        $data['author']=empty($author)?'':htmlentities($author,ENT_NOQUOTES,"utf-8");
        $data['content']=empty($content)?'':$content;
        if(!empty($picurl)) $data['picurl']=$picurl;
        if($this->db->modify('text_news_pic',$id,$data)){
            return true;
        }else return false;
    }

    public function delete($id){
        $id=(int)$id;
        if($this->db->hide('text_news_pic', $id)){
            return true;
        }else return false;
    }
} 