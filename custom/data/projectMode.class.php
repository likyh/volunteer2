<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-4-30
 * Time: 下午6:32
 */

/**
 * Class projectMode
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class projectMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    public function getList($rows,$offset=0){
        $sql="SELECT `id`, `name`, `time`, `place`, `organizer`, `content`, `picurl`
              FROM  text_activity where `enable`=1 order by `create_time` DESC limit $offset,$rows";
        $result=$this->db->getAll($sql);
        return $result;
    }
    public function getRowsTotal(){
        $countSql="select count(1) from text_activity where `enable`=1";
        return $this->db->getValue($countSql);
    }

    public function content($id){
        $id=(int)$id;
        $sql="select `id`, `name`, `time`, `place`, `organizer`, `content`, `description`, `picurl` from `text_activity`
              where `id`=$id";
        $result=$this->db->getOne($sql);
        //获取上一篇下一篇  $result['preId'],$result['nextId']
        $total=$this->db->getValue("select count(*) from `text_activity` where `enable`=1");
        $getAllDesc="SELECT `id`,`name` FROM `text_activity` WHERE `enable`=1  order by `create_time` DESC";
        $re=$this->db->getAll($getAllDesc);
        for($i=0;$i<$total;$i++){
            if($re[$i]['id']==$id){
                if($i==0){
                    $result['preId']=$result['preName']=NULL;
                }else{
                    $result['preId']=$re[$i-1]['id'];
                    $result['preName']=$re[$i-1]['name'];
                }
                if($i==$total-1){
                    $result['nextId']=$result['nextName']=NULL;
                }else{
                    $result['nextId']=$re[$i+1]['id'];
                    $result['nextName']=$re[$i+1]['name'];
                }
            }
        }
        return $result;
    }

    public function createSubmit($name,$organizer,$time,$place,$content,$picurl){
        $data['name']=empty($name)?'':htmlentities($name,ENT_NOQUOTES,"utf-8");
        $data['organizer']=empty($organizer)?'':htmlentities($organizer,ENT_NOQUOTES,"utf-8");
        $data['time']=empty($time)?'':htmlentities($time,ENT_NOQUOTES,"utf-8");
        $data['place']=empty($place)?'':htmlentities($place,ENT_NOQUOTES,"utf-8");
        $data['content']=empty($content)?'':$content;
        $data['picurl']=empty($picurl)?'':$picurl;
        if($this->db->insert('text_activity', $data)){
            return true;
        }else return false;
    }

    public function modifyShow($id){
        $id=(int)$id;
        $sql="select `id`, `name`, `organizer`,`time`,`place`,`content`,`picurl` from `text_activity` where `id`=$id";
        $result=$this->db->getOne($sql);
        return $result;
    }

    public function modifySubmit($id,$name,$organizer,$time,$place,$content,$picurl){
        $id=(int)$id;
        $data['name']=empty($name)?'':htmlentities($name,ENT_NOQUOTES,"utf-8");
        $data['organizer']=empty($organizer)?'':htmlentities($organizer,ENT_NOQUOTES,"utf-8");
        $data['time']=empty($time)?'':htmlentities($time,ENT_NOQUOTES,"utf-8");
        $data['place']=empty($place)?'':htmlentities($place,ENT_NOQUOTES,"utf-8");
        $data['content']=empty($content)?'':$content;
        $data['picurl']=empty($picurl)?'':$picurl;
        if($this->db->modify('text_activity',$id, $data)){
            return true;
        }else return false;
    }

    public function delete($id){
        $id=(int)$id;
        if($this->db->hide('text_activity', $id)){
            return true;
        }else return false;
    }
    public function getType(){
        $result=$this->db->getAll("SELECT `admin_user_id` as `id`,`nickname` as `type`
        FROM  `admin_user`");
        return $result;
    }
    public function getState(){
        $result=$this->db->getAll("SELECT * FROM  `activity_state`");
        return $result;
    }
}