<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-4-30
 * Time: 上午8:07
 */

/**
 * Class activityMode
 * activity,time,usertime删除都直接delete
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class activityMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }
    public function activityList($rows,$offset=0,$org=null,$state=null,$time=null,$s=null){  // limit $rows,$offset
        $rows=(int)$rows;
        $offset=(int)$offset;
        switch($org){
            case 'school':$whereOrg=' and `a`.`typeid`=1 ';break;
            case 'academy':$whereOrg=' and `c`.`nickname` like '.$this->db->quote("%院%青协%");break;
            case 'others':$whereOrg=' and `c`.`nickname` like '.$this->db->quote("%团委%");break;
            case 'all':default:$whereOrg='';break;
        }
        switch($state){
            case 1:$whereState=' and `a`.`state_id`=1 ';break;
            case 2:$whereState=' and `a`.`state_id`=2 ';break;
            case 3:$whereState=' and `a`.`state_id`=3 ';break;
            case 'all':default:$whereState='';break;
        }
        $today=date('Y-m-d');
        switch($time){
            case 'week':$lastTime=date('Y-m-d',strtotime("last week"));break;
            case 'month':$lastTime=date('Y-m-d',strtotime("last month"));break;
            case '3month':$lastTime=date('Y-m-d',strtotime("3 month ago"));break;//
            case 'year';$lastTime=date('Y-m-d',strtotime("last year"));break;
            case 'all':default:$lastTime='';break;
        }
        if($lastTime!=0){
            $whereTime=' and `a`.`create_time`'." between '{$lastTime}' and '{$today}' ";
        }else{
            $whereTime='';
        }
        if(!empty($s)){
            $whereS=' and `a`.`name` like '.$this->db->quote("%{$s}%");
        }else {
            $whereS='';
        }
        $sql="select `a`.`id`,`a`.`name`,`c`.`nickname` as `author`,`b`.`name` as `state`,`a`.`time`
                from `activity` as `a`,`activity_state` as `b`,`admin_user` as `c`
                where `a`.`typeid`=`c`.`id` and c.`group_id`<>4
                and `a`.`state_id`=`b`.`id`
                and `a`.`enable`=1
                and `a`.`auth`=1".$whereOrg.$whereState.$whereTime.$whereS." order by `a`.`create_time` desc limit $offset,$rows";

        $result=$this->db->getAll($sql);
        return $result;
    }
    public function getRowsTotal($org=null,$state=null,$time=null,$s=null){
        switch($org){
            case 1:$whereOrg=' and `a`.`typeid`=1 ';break;
            case 2:$whereOrg=' and `a`.`typeid`!=1 ';break;
            case 'all':default:$whereOrg='';break;
        }
        switch($state){
            case 1:$whereState=' and `a`.`state_id`=1 ';break;
            case 2:$whereState=' and `a`.`state_id`=2 ';break;
            case 3:$whereState=' and `a`.`state_id`=3 ';break;
            case 'all':default:$whereState='';break;
        }
        $today=date('Y-m-d');
        switch($time){
            case 'week':$lastTime=date('Y-m-d',strtotime("last week"));break;
            case 'month':$lastTime=date('Y-m-d',strtotime("last month"));break;
            case '3month':$lastTime=date('Y-m-d',strtotime("3 month ago"));break;//
            case 'year';$lastTime=date('Y-m-d',strtotime("last year"));break;
            case 'all':default:$lastTime='';break;
        }
        if(!empty($lastTime)){
            $whereTime=' and `a`.`create_time`'." between '{$lastTime}' and '{$today}' ";
        }else{
            $whereTime='';
        }
        if(!empty($s)){
            $whereS=' and `a`.`name` like '.$this->db->quote("%{$s}%");
        }else {
            $whereS='';
        }
        $countSql="select count(1) from `activity` as `a`,`admin_user` as `c`,`activity_state` as `b`
        where `auth`=1
        and `a`.`typeid`=`c`.`id` and c.`group_id`<>4
        and `a`.`state_id`=`b`.`id`
        and `a`.`enable`=1 ".$whereOrg.$whereState.$whereTime.$whereS;
        return $this->db->getValue($countSql);
    }
    public function activityContent($id){
        $id=(int)$id;
        $sql="select `a`.`id`, `a`.`name`,`c`.`nickname` as `author`,`b`.`name` as `state`,`organizer`, `nature`, `place`, `time`, `picurl`,
            `attachment`, `description`, `content`
              from `activity` as `a`,`activity_state` as `b`,`admin_user` as `c`
              where `a`.`state_id`=`b`.`id`
              and `a`.`typeid`=`c`.`admin_user_id` and c.`group_id`<>4
              and `a`.`id`=$id";
        $result['content']=$this->db->getOne($sql);
        //获取上一篇下一篇  $result['preId'],$result['nextId']
        $total=$this->db->getValue("select count(*) from `activity` where `auth`=1 and `enable`=1");
        $getAllDesc="SELECT `id`,`name` FROM `activity` WHERE `auth`=1 and `enable`=1  order by `create_time` DESC";
        $re=$this->db->getAll($getAllDesc);
        for($i=0;$i<$total;$i++){
            if($re[$i]['id']==$id){
                if($i==0){
                    $result['preId']=$result['preName']=NULL;
                }else{
                    $result['preId']=$re[$i-1]['id'];
                    $result['preName']=$re[$i-1]['name'];
                }
                if($i==$total-1){
                    $result['nextId']=$result['nextName']=NULL;
                }else{
                    $result['nextId']=$re[$i+1]['id'];
                    $result['nextName']=$re[$i+1]['name'];
                }
            }
        }
        //获得该活动的所有时间段
        $timeSql="select `id`,`start`,`end` from activity_time
                  where `activity_id`=$id and `expired`='0'
                  order by `id` desc";
        $result['time']=$this->db->getAll($timeSql);
        return $result;
    }
    public function activityCreateSubmit($name,$typeid,$state_id,$nature,$organizer,$time,$place,$description,$content,$picurl){
        $data['name']=empty($name)?'':$name;
        //先检查是否有重名活动
        $checkIfExist="select * from activity where `name`='{$data['name']}'";
        $checkResult=$this->db->getExist($checkIfExist);
        if($checkResult) return false;
        $data['typeid']=empty($typeid)?'':$typeid;
        $data['state_id']=empty($state_id)?'':(int)($state_id);
        $data['nature']=empty($nature)?'':$nature;
        $data['organizer']=empty($organizer)?'':$organizer;
        $data['time']=empty($time)?'':$time;
        $data['place']=empty($place)?'':$place;
        $data['description']=empty($description)?'':$description;
        $data['content']=empty($content)?'':$content;
        $data['picurl']=empty($picurl)?'':$picurl;
        $data['attachment']=empty($attachment)?'':$attachment;
        if($this->db->insert('activity', $data)){
            return true;
        }else return false;
    }

    /**
     * @param $id
     * @return array
     */
    public function activityModifyShow($id){
        $id=(int)$id;
        $sql="select `name`, `typeid`, `id`, `organizer`, `nature`, `state_id`, `place`, `time`, `picurl`, `attachment`, `description`, `content`
              from `activity`
              where `id`=$id";
        $result=$this->db->getOne($sql);
        return $result;
    }

    public function activityModifySubmit($id,$name,$state_id,$nature,$organizer,$time,$place,$description,$content,$picurl,$attachment=null){
        $id=(int)$id;
        $data['name']=empty($name)?'':$name;
        $data['state_id']=empty($state_id)?'':(int)($state_id);
        $data['nature']=empty($nature)?'':$nature;
        $data['organizer']=empty($organizer)?'':$organizer;
        $data['time']=empty($time)?'':$time;
        $data['place']=empty($place)?'':$place;
        $data['description']=empty($description)?'':$description;
        $data['content']=empty($content)?'':$content;
        if(!empty($picurl)) $data['picurl']=$picurl;
        $result=$this->db->update('activity', $id, $data);
        if($result){
            return true;
        }else return false;
    }

    public function getExist($id){
        $sql="select * from activity_time where activity_id=$id";
        $exist=$this->db->getExist($sql);
        if($exist){
            return true;
        }else return false;
    }

    public function activityDelete($id){
        $id=(int)$id;
        if($this->db->delete('activity', $id)){
            return true;
        }else return false;
    }

    public function activityAuth($id){
        $id=(int)$id;
        $sql="UPDATE `activity` set `auth`='1' where `id`=$id ";
        if($this->db->sqlExec($sql)){
            return true;
        }else return false;
    }

    public function getTimeTotal($id){
        $countSql="select count(*) from `activity_time`
              where `activity_id`=$id";
        return $this->db->getValue($countSql);
    }

    public function timeShow($id){
        $id=(int)$id;
        $sql="select `id`,`start`,`end`,`length`,`total_num`,`existing_num`,`expired` from `activity_time`
              where `activity_id`=$id order by `start` desc";
        $result=$this->db->getAll($sql);
        return $result;
    }

    public function timeCreateSubmit($id,$date,$startTime,$endTime,$length,$total_num){
        $data['activity_id']=(int)$id;
        $date=empty($date)?'':htmlentities($date,ENT_NOQUOTES,"utf-8");
        $data['start']=date("Y-m-d H:i",strtotime("{$date} {$startTime}"));
        $data['end']=date("Y-m-d H:i",strtotime("{$date} {$endTime}"));
        $data['length']=isset($length)?(int)($length):0;
        $data['total_num']=isset($total_num)?(int)($total_num):0;
        if($this->db->insert('activity_time', $data)){
            return true;
        }else return false;
    }

    public function timeDelete($id){
        $id=(int)$id;
        if($this->db->delete('activity_time', $id)){
            return true;
        }else return false;
    }

    public function getUserTotal($id){
        $countSql="select count(*) from `activity_user` as `a`,`user_info` as `b`,
        `studentinfo` as `c`,`activity_time` as `d` where `a`.`userid`=`b`.`id`
        and `d`.`id`=`a`.`timeid` and `b`.`num`=`c`.`num` and `timeid`=$id and
        `a`.`enable`=1 and `b`.`enable`=1 and `c`.`enable`=1 and `d`.`enable`=1 ";
        return $this->db->getValue($countSql);
    }

    public function userShow($id,$rows=20,$offset=0){
        $id=(int)$id;  //活动时间id
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select `a`.`id`,`timeid`,`c`.`name`,`c`.`num`,`b`.`tel`,`c`.`IDnum`,`checked`,`rejected`,`d`.`length` ,`d`.`start`,`d`.`expired`
            from `activity_user` as `a`,`user_info` as `b`,`studentinfo` as `c`,`activity_time` as `d`
            where `a`.`userid`=`b`.`id`
            and `d`.`id`=`a`.`timeid`
            and `b`.`num`=`c`.`num`
            and `timeid`=$id limit {$offset},{$rows}";
        $result=$this->db->getAll($sql);
        return $result;
    }

    public function userAdd($id,$num,$name){
        $id=(int)$id;  //活动时间id
        //判断该时间是否过期
        $ifExpired=$this->db->getValue("select `expired` from `activity_time` where `id`=$id");
        if($ifExpired=='1') return false;
        $num=$this->db->quote(isset($num)?($num):'');
        $name=$this->db->quote(isset($name)?($name):'');
        //判断是否有该学生
        $stuExist="select * from `studentinfo` where `num`=$num and `name`=$name";
        if($this->db->getExist($stuExist)){
            //判断用户是否已经注册
            $userExist="SELECT `id` FROM `user_info` where `num`={$num} limit 1";
            if($this->db->getExist($userExist)){
                //再判断用户是否已经报名
                $timeUserCheck="select 1 from `activity_user`
                            where `userid`=(SELECT id FROM `user_info` where `num`={$num})
                            and {$name}=(select `name` from `studentinfo` where `num`={$num})
                            and `timeid`={$id};";
                if($this->db->getExist($timeUserCheck)){
                    $result['state']='false';
                    $result['info']='此用户已报名 ';
                }else{
                    $addUser="insert into `activity_user`(`userid`,`timeid`)values
                ((SELECT id FROM `user_info` where `num`={$num}),'$id');";
                    if($this->db->sqlExec($addUser)){
                        $result['state']='success';
                        $result['info']='报名者添加成功 ';
                        $sql="update activity_time set `existing_num`=`existing_num`+1 where `id`=$id";
                        $this->db->query($sql);
                    }
                }
            }else{
                $result['state']='false';
                $result['info']='该报名者未注册';
            }
        }else{
            $result['state']='false';
            $result['info']='该报名者信息不正确 ';
        }
        return $result;
    }

    public function userAuth($id){
        $id=(int)$id;
        $sql="UPDATE activity_user set `checked`='1' where `id`={$id}";
        if($this->db->sqlExec($sql)){
            return true;
        }else return false;
    }
    public function reject($id){
        $id=(int)$id;
        $sql="UPDATE activity_user set `rejected`='1' where `id`={$id}";
        if($this->db->sqlExec($sql)){
            return true;
        }else return false;
    }
    public function userDelete($id,$time_id){
        $id=(int)$id;
        $sql="delete from `activity_user` where `id`={$id}";
        if($this->db->sqlExec($sql)){
            $sql="update activity_time set `existing_num`=`existing_num`-1 where `id`=$time_id";
            $this->db->query($sql);
            return true;
        }else return false;

    }
    //暂时没用，搜索框可能要用的
    public function activityAuthor(){
        $result=$this->db->getAll("SELECT `admin_user_id` as `id`,`nickname` as `name` FROM `admin_user`");
        return $result;
    }
    //TODO $admin_user_id只查出此用户的活动
    public function adminList($rows,$offset=0,$admin_user_id=null){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select `a`.`id`, `a`.`name`, `au`.`nickname` as `author`, `a`.`organizer`,`b`.`name` as `state`, `a`.`place`, `time`, `auth`
        from `activity` as `a`,`admin_user` as `au`,`activity_state` as `b`
        where `a`.`typeid`=`au`.`admin_user_id`
        and `a`.`state_id`=`b`.`id`
        and `a`.`enable`=1 order by `a`.`create_time` desc limit $offset,$rows";
        $result=$this->db->getAll($sql);
        return $result;
    }
    public function getTotal($admin_user_id){
        $admin_user_id=$this->db->quote($admin_user_id);
        $id=$this->db->getValue("select `id` from `admin_user` where `admin_user_id`=$admin_user_id");
        $sql="select count(*)
        from `activity` as `a`,`admin_user` as `au`,`activity_state` as `b`
        where `a`.`typeid`=`au`.`admin_user_id`
        and `a`.`state_id`=`b`.`id`
        and `a`.`enable`=1 and `au`.`id`=$id";
        $result=$this->db->getValue($sql);
        return $result;
    }
    public function adminPartList($rows,$offset=0,$admin_user_id){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $admin_user_id=$this->db->quote($admin_user_id);
        $id=$this->db->getValue("select `id` from `admin_user` where `admin_user_id`=$admin_user_id");
        $sql="select `a`.`id`, `a`.`name`, `au`.`nickname` as `author`, `a`.`organizer`,`b`.`name` as `state`, `a`.`place`, `time`, `auth`
        from `activity` as `a`,`admin_user` as `au`,`activity_state` as `b`
        where `a`.`typeid`=`au`.`admin_user_id`
        and `a`.`state_id`=`b`.`id`
        and `a`.`enable`=1 and `au`.`id`=$id order by `a`.`create_time` desc limit $offset,$rows";
        $result=$this->db->getAll($sql);
        return $result;
    }

    public function state(){
        $sql="select `id`, `name` from `activity_state`";
        return $this->db->getAll($sql);
    }
    //用户报名
    public function apply($activity_id,$userid,$timeid){
        $activity_id=(int)$activity_id;
        $userid=(int)$userid;
        $timeid=(int)$timeid;
        $sql="select 1 from `activity_user` where `userid`=$userid and `timeid`=$timeid";
        $checkIfExist=$this->db->getExist($sql);
        if($checkIfExist){
            $result['state']='您已经报过名了，试试别的吧';
            $result['flag']=false;
        }else{
            $sql="select `existing_num`,`total_num` from activity_time where `id`=$timeid and `activity_id`=$activity_id";
            $checkNum=$this->db->getOne($sql);
//            var_dump($checkNum);
            if($checkNum['existing_num']==$checkNum['total_num']){
                $result['state']='报名人数已满';
            }else{
                $data['activity_id']=$activity_id;
                $data['userid']=$userid;
                $data['timeid']=$timeid;
                $re=$this->db->insert('activity_user',$data);
                if($re){
                    $result['state']='报名成功';
                    $result['flag']=true;
                    $sql="update activity_time set `existing_num`=`existing_num`+1 where `id`=$timeid";
                    $this->db->query($sql);
                }else{
                    $result['state']='报名失败请重试';
                    $result['flag']=false;
                }
            }

        }
        return $result;
    }
    public function setUserActId($id){
        $_SESSION['user_act_id']=$id;
    }
    public function getUserActId(){
        return isset($_SESSION['user_act_id'])?$_SESSION['user_act_id']:null;
    }
    public function setTimeActId($id){
        $_SESSION['time_act_id']=$id;
    }
    public function getTimeActId(){
        return isset($_SESSION['time_act_id'])?$_SESSION['time_act_id']:null;
    }

} 




