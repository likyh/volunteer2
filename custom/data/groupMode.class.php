<?php
/**
 * Created by PhpStorm.
 * author: sg
 * Date: 14-4-29
 * Time: 下午9:09
 */

/**
 * Class groupMode
 * ▲▲表示前后台通用的函数
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class groupMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    /**
     * ▲▲公益团体列表,前台后台都可以用这个
     * @return mixed
     */

    public function getList($rows,$offset=0){
        $listSql="select `id`, `title` from text_group where `enable`=1 order by `title` limit $offset,{$rows}  ";
        $result=$this->db->getAll($listSql);
        return $result;
    }

    public function getRowsTotal(){
        $countSql="select count(1) from text_group where `enable`=1";
        return $this->db->getValue($countSql);
    }

    public function content($id){
        $id=(int)$id;
        $contentSql="select `id`, `title`,`content` from `text_group` where `enable`=1 and `id`=$id";
        $result=$this->db->getOne($contentSql);
        //获取上一篇下一篇  $result['preId'],$result['nextId']
        $total=$this->db->getValue("select count(*) from `text_group` where `enable`=1");
        $getAllDesc="SELECT `id`, `title` FROM `text_group` WHERE `enable`=1  order by `create_time` DESC";
        $re=$this->db->getAll($getAllDesc);
        for($i=0;$i<$total;$i++){
            if($re[$i]['id']==$id){
                if($i==0){
                    $result['preId']=$result['preTitle']=NULL;
                }else{
                    $result['preId']=$re[$i-1]['id'];
                    $result['preTitle']=$re[$i-1]['title'];
                }
                if($i==$total-1){
                    $result['nextId']=$result['nextTitle']=NULL;
                }else{
                    $result['nextId']=$re[$i+1]['id'];
                    $result['nextTitle']=$re[$i+1]['title'];
                }
            }
        }
        return $result;
    }
    public function delete($id){
        $id=(int)$id;
        if($this->db->hide('text_group', $id)){
            return true;
        }else return false;
    }

    public function modifyShow($id){
        $id=(int)$id;
        $sql="select `id`,`title`,`content` from `text_group` where `id`=$id";
        $result['ModifyShow']=$this->db->getOne($sql);
        return $result;
    }

    public function modifySubmit($id,$title,$content){
        $id=(int)$id;
        $data['content']=empty($content)?'':$content;
        $data['title']=empty($title)?'':htmlentities($title,ENT_NOQUOTES,"utf-8");
        $result=$this->db->modify('text_group', $id, $data);
        if($result){
            return true;
        }else return false;
    }

    public function createSubmit($title,$content){
        $data['title']=empty($title)?'':htmlentities($title,ENT_NOQUOTES,"utf-8");
        $data['content']=empty($content)?'':htmlentities($content,ENT_NOQUOTES,"utf-8");
        if($this->db->insert('text_group',$data)){
            return true;
        }else return false;

    }

} 