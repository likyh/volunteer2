<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-4-30
 * Time: 上午7:21
 */
/**
 * Class downloadMode
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class downloadMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    public function getList($rows=20,$offset=0){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="select `id`,`title`,`create_time` from `text_download`
                where `enable`=1 order by `create_time` desc limit {$offset},{$rows}";
        $result=$this->db->getAll($sql);
        return $result;
    }
    public function getRowsTotal(){
        $countSql="select count(1) from text_download where `enable`=1";
        return $this->db->getValue($countSql);
    }
    public function content($id){
        $id=(int)$id;
        $contentSql="select `id`, `title`,`url` from `text_download` where `enable`=1 and `id`=$id";
        $result=$this->db->getOne($contentSql);
        //获取上一篇下一篇  $result['preId'],$result['nextId']
        $total=$this->db->getValue("select count(*) from `text_download` where `enable`=1");
        $getAllDesc="SELECT `id`,`title` FROM `text_download` WHERE `enable`=1  order by `create_time` DESC";
        $re=$this->db->getAll($getAllDesc);
        for($i=0;$i<$total;$i++){
            if($re[$i]['id']==$id){
                if($i==0){
                    $result['preId']=$result['preTitle']=NULL;
                }else{
                    $result['preId']=$re[$i-1]['id'];
                    $result['preTitle']=$re[$i-1]['title'];
                }
                if($i==$total-1){
                    $result['nextId']=$result['nextTitle']=NULL;
                }else{
                    $result['nextId']=$re[$i+1]['id'];
                    $result['nextTitle']=$re[$i+1]['title'];
                }
            }
        }
        return $result;
    }
    public function createSubmit($title,$url){
        $data['title']=empty($title)?'':htmlentities($title,ENT_NOQUOTES,"utf-8");
        $data['url']=empty($url)?'':$this->db->quote($url);
        if($this->db->insert('text_download',$data)){
            return true;
        }else return false;
    }

    public function delete($id){
        $id=(int)$id;
        if($this->db->hide('text_download', $id)){
            return true;
        }else return false;
    }

    public function modifyShow($id){
        $id=(int)$id;
        $sql="select `id`,`title`,`url` from `text_download` where `id`=$id";
        $result=$this->db->getOne($sql);
        return $result;
    }

    public function modifySubmit($id,$title,$url){
        $id=(int)$id;
        $data['url']=empty($url)?'':$this->db->quote($url);
        $data['title']=empty($title)?'':htmlentities($title,ENT_NOQUOTES,"utf-8");
        $result=$this->db->modify('text_download', $id, $data);
        if($result){
            return true;
        }else return false;
    }

} 