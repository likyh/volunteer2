<?php
/**
 * Created by PhpStorm.
 * author: sg
 * Date: 14-4-29
 * Time: 下午8:09
 */
/**
 * Class newsMode
 * ▲▲表示前后台通用的函数
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class newsMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    public function getList($rows,$offset=0,$type=null,$s=null){
        switch($type){
            case 'xqx':$whereType=' and `typeid`=1 ';break;
            case 'other':$whereType=' and `typeid`!=1 ';break;
            case 'all':default:$whereType='';break;
        }
        if($s){
            $whereS=' and `title` like '.$this->db->quote("%{$s}%");
        }else{
            $whereS='';
        }
        $listSql="SELECT `text_news`.`id`, `nickname` as `type`, `title`, `date`
        FROM `text_news`,`admin_user` where `text_news`.`typeid`=`admin_user`.`id`
                  AND `admin_user`.`group_id`!=5
                  AND `text_news`.`enable`=1
                  AND `text_news`.`auth`=1".$whereType.$whereS." ORDER BY `date` DESC limit $offset,$rows";
        $result=$this->db->getAll($listSql);
        return $result;
    }

    public function getRowsTotal($type=null){
        switch($type){
            case 'xqx':$whereType=' and `typeid`=1 ';break;
            case 'other':$whereType=' and `typeid`!=1 ';break;
            case 'all':default:$whereType='';break;
        }
        $countSql="select count(1) from `text_news` where `auth`=1 and `enable`=1 {$type}";
        return $this->db->getValue($countSql);
    }

    public function content($id){
        $id=(int)$id;
        $contentSql="SELECT `id`, `title`, `author`, `date`, `content`
        FROM `text_news` where `id`=$id;";
        $result=$this->db->getOne($contentSql);
        //获取上一篇下一篇  $result['preId'],$result['nextId']
        $total=$this->getRowsTotal();
        $getAllDesc="SELECT `id`,`title` FROM `text_news` WHERE `auth`=1 and `enable`=1  order by `date` DESC";
        $re=$this->db->getAll($getAllDesc);
        for($i=0;$i<$total;$i++){
            if($re[$i]['id']==$id){
                if($i==0){
                    $result['preId']=$result['preTitle']=NULL;
                }else{
                    $result['preId']=$re[$i-1]['id'];
                    $result['preTitle']=$re[$i-1]['title'];
                }
                if($i==$total-1){
                    $result['nextId']=$result['nextTitle']=NULL;
                }else{
                    $result['nextId']=$re[$i+1]['id'];
                    $result['nextTitle']=$re[$i+1]['title'];
                }
            }
        }
        return $result;
    }

    public function adminNewsList($rows=20,$offset=0){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $sql="SELECT `text_news`.`id`,`author`,`title`, `date`,`nickname` as `type` FROM `text_news`,`admin_user`
                  WHERE `admin_user`.`admin_user_id`=`text_news`.`typeid`
                  AND   `text_news`.`enable`=1  ORDER BY `text_news`.`id` DESC limit {$offset},{$rows}";
        $result=$this->db->getAll($sql);
        return $result;
    }

    public function subNewsList($rows,$offset,$admin_user_id){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $admin_user_id=$this->db->quote($admin_user_id);
        $id=$this->db->quote($this->db->getValue("select `id` from `admin_user` where `admin_user_id`=$admin_user_id")) ;
        $s['list']=$this->db->getAll("select `admin_user_id` from `admin_user` where `id`=$id");
        $admin_id1=$this->db->quote($s['list'][0]['admin_user_id']);
        $admin_id2=$this->db->quote($s['list'][1]['admin_user_id']);
        $sql="SELECT `text_news`.`id`,`author`,`title`, `date`,`nickname` as `type` FROM `text_news`,`admin_user`
                  WHERE `admin_user`.`admin_user_id`=`text_news`.`typeid`
                  AND   `text_news`.`enable`=1 and `text_news`.`typeid` in ($admin_id1,$admin_id2)  ORDER BY `text_news`.`id` DESC limit {$offset},{$rows}";
        $result=$this->db->getAll($sql);
        return $result;
    }
    public function getSubRowsTotal($admin_user_id){
        $admin_user_id=$this->db->quote($admin_user_id);
        $id=$this->db->quote($this->db->getValue("select `id` from `admin_user` where `admin_user_id`=$admin_user_id"));
        $s['list']=$this->db->getAll("select `admin_user_id` from `admin_user` where `id`=$id");
        $admin_id1=$this->db->quote($s['list'][0]['admin_user_id']);
        $admin_id2=$this->db->quote($s['list'][1]['admin_user_id']);
        $sql="SELECT count(*) FROM `text_news`,`admin_user`
                  WHERE `admin_user`.`admin_user_id`=`text_news`.`typeid`
                  AND   `text_news`.`enable`=1 and `text_news`.`typeid` in ($admin_id1,$admin_id2) ";
        $result=$this->db->getValue($sql);
        return $result;
    }

    public function createSubmit($title,$author,$date,$typeid,$content){
        $data['title']=empty($title)?'':htmlentities($title,ENT_NOQUOTES,"utf-8");
        //先检查是否有重名新闻
        $checkIfExist="select * from text_news where `title`='{$data['title']}'";
        $checkResult=$this->db->getExist($checkIfExist);
        if($checkResult==1) return false;
        $data['typeid']=$typeid;
        $data['date']=empty($date)?'':htmlentities($date,ENT_NOQUOTES,"utf-8");
        $data['author']=empty($author)?'':htmlentities($author,ENT_NOQUOTES,"utf-8");
        $data['content']=empty($content)?'':$content;
        if($this->db->insert('text_news',$data)){
            return true;
        }else return false;
    }

    public function auth($id){
        $id=(int)$id;
        $sql="UPDATE text_news set `auth`='1' where `id`=$id ";
        if($this->db->sqlExec($sql)){
            return true;
        }else return false;

    }

    public function delete($id){
        $id=(int)$id;
        if($this->db->hide('text_news', $id)){
            return true;
        }else return false;
    }

    public function modifyShow($id){
        $id=(int)$id;
        $sql="select `text_news`.`id`,`typeid`,`nickname` as `type`,`title`,`author`,`date`,`content` from `text_news`,`admin_user`
               where `text_news`.`typeid`=`admin_user`.`admin_user_id` and `text_news`.`id`=$id";
        $result=$this->db->getOne($sql);
        return $result;
    }

    public function modifySubmit($id,$title,$author,$date,$typeid,$content){
        $id=(int)$id;
        $data['title']=empty($title)?'':htmlentities($title,ENT_NOQUOTES,"utf-8");
        $data['date']=empty($date)?'':htmlentities($date,ENT_NOQUOTES,"utf-8");
        $data['author']=empty($author)?'':htmlentities($author,ENT_NOQUOTES,"utf-8");
        $data['content']=empty($content)?'':$content;
        $data['typeid']=(int)$typeid;
        $result=$this->db->update('text_news', $id, $data);
        if($result){
            return true;
        }else return false;
    }
}