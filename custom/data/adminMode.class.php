<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-5-3
 * Time: 下午7:49
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
import('Lib.Data.SimpleSession');
class adminMode extends Data {
    /** @var SqlDB*/
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
        SimpleSession::init();
    }
    public function login($user,$pass){
        $pass=$this->db->quote(getPassWord($user, $pass));
        $user=$this->db->quote($user);
        $sql="SELECT * FROM `admin_user` where (`login_name`={$user} and `code`={$pass})";
        $ifLoginSucc=$this->db->getExist($sql);
        if($ifLoginSucc){
            // 查询用户信息
            $sql="SELECT `a`.`admin_user_id` as `typeid`,`a`.`id`,`a`.`login_name` as `user`,`a`.`nickname` as `name`,`a`.`group_id`,`b`.`name` as `group_name`,`b`.`title` as `group_title`
                  FROM `admin_user` as `a`,`manager_group` as `b`
                  where (`login_name`={$user}) and `a`.`group_id` = `b`.`id` LIMIT 1";
            $_SESSION['admin']['user']=$this->db->getOne($sql);
            // 记录已经登录
            $_SESSION['admin']['login']=true;
        }
        return $ifLoginSucc;
    }
    public function checkLogin(){
        return isset($_SESSION['admin']['login'])&&$_SESSION['admin']['login']&&!empty($_SESSION['admin']['user']);
    }
    public function logout(){
        SimpleSession::release();
    }
    public function checkAuth($textType,$operateType){
        return getConfig("xtw",$textType."_".$operateType,"auth");
    }
    public function getName(){
        return $this->checkLogin()?$_SESSION['admin']['user']['user']:null;
    }
    public function getCName(){
        return $this->checkLogin()?$_SESSION['admin']['user']['name']:null;
    }
    public function getTypeId(){
        return $this->checkLogin()?$_SESSION['admin']['user']['typeid']:null;
    }
    public function check_auth($user_id){
        if(($user_id==1)||($user_id==3)){
            $result=true;
        }else{
            $result=false;
        }
        return $result;
    }
    public function getLevel($admin_user_id){
        if($admin_user_id==1||$admin_user_id==3){
            $result=true;
        }else{
            $admin_user_id=$this->db->quote($admin_user_id);
            $sql="select (1) from `admin_user` where `admin_user_id`=$admin_user_id and `group_id`=5";
            $result=$this->db->getExist($sql);
        }
        return $result;
    }
}