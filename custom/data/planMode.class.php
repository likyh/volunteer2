<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-4-30
 * Time: 上午7:21
 */

/**
 * Class planMode
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class planMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    public function getList($rows,$offset=0){
        $rows=(int)$rows;
        $offset=(int)$offset;
        $listSql="select `id`, `name` as `title`,`description`, `time`, `place`, `organizer` from text_plan where `enable`=1 order by `create_time` DESC limit $offset,{$rows}";
        $result=$this->db->getAll($listSql);
        return $result;
    }
    public function getRowsTotal(){
        $countSql="select count(1) from text_plan where `enable`=1";
        return $this->db->getValue($countSql);
    }
    public function content($id){
        $id=(int)$id;
        $contentSql="select `id`, `name` as `title`,`content`,`description`, `time`, `place`, `organizer`, `picurl` from `text_plan` where `enable`=1 and `id`=$id";
        $result=$this->db->getOne($contentSql);
        //获取上一篇下一篇  $result['preId'],$result['nextId']
        $total=$this->db->getValue("select count(*) from `text_plan` where `enable`=1");
        $getAllDesc="SELECT `id`,`name` as `title` FROM `text_plan` WHERE `enable`=1  order by `create_time` DESC";
        $re=$this->db->getAll($getAllDesc);
        for($i=0;$i<$total;$i++){
            if($re[$i]['id']==$id){
                if($i==0){
                    $result['preId']=$result['preTitle']=NULL;
                }else{
                    $result['preId']=$re[$i-1]['id'];
                    $result['preTitle']=$re[$i-1]['title'];
                }
                if($i==$total-1){
                    $result['nextId']=$result['nextTitle']=NULL;
                }else{
                    $result['nextId']=$re[$i+1]['id'];
                    $result['nextTitle']=$re[$i+1]['title'];
                }
            }
        }
        return $result;
    }

    public function delete($id){
        $id=(int)$id;
        if($this->db->hide('text_plan', $id)){
            return true;
        }else return false;
    }

    public function modifyShow($id){
        $id=(int)$id;
        $sql="select `id`,`name` as `title`,`content` from `text_plan` where `id`=$id";
        $result['ModifyShow']=$this->db->getOne($sql);
        return $result;
    }

    public function modifySubmit($id,$name,$content,$description,$time,$place,$organizer,$picurl){
        $id=(int)$id;
        $data['content']=empty($content)?'':$content;
        $data['name']=empty($name)?'': $name;
        $data['description']=empty($description)?'': $description;
        $data['time']=empty($time)?'': $time;
        $data['place']=empty($place)?'': $place;
        $data['organizer']=empty($organizer)?'': $organizer;
        if(!empty($picurl)) $data['picurl']=$picurl;
        $result=$this->db->modify('text_plan', $id, $data);
        if($result){
            return true;
        }else return false;
    }

    public function createSubmit($name,$content,$description,$time,$place,$organizer,$picurl){
        $data['name']=empty($name)?'':$name;
        //先检查是否有重名新闻
        $checkIfExist="select * from text_plan where `name`='{$data['name']}'";
        $checkResult=$this->db->getExist($checkIfExist);
        if($checkResult) return false;
        $data['content']=empty($content)?'':$content;
        $data['description']=empty($description)?'':$description;
        $data['time']=empty($time)?'':$time;
        $data['place']=empty($place)?'':$place;
        $data['organizer']=empty($organizer)?'':$organizer;
        if(!empty($picurl)) $data['picurl']=$picurl;
        if($this->db->insert('text_plan',$data)){
            return true;
        }else return false;
    }


} 