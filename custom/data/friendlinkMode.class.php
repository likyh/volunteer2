<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-4-30
 * Time: 下午7:21
 */

/**
 * Class friendlinkMode
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class friendlinkMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getList($rows,$offset=0){
        $sql="select `id`, `name`, `url`, `picurl` from `text_friendlink` where `enable`=1 limit $offset,{$rows}";
        $result=$this->db->getAll($sql);
        return $result;
    }

    /**
     * @param $name
     * @param $url
     * @param $picurl
     * @return bool
     */
    public function createSubmit($name,$url,$picurl){
        $data['name']=empty($name)?'':$name;
        $data['url']=empty($url)?'':$url;
        $data['picurl']=empty($picurl)?'':$picurl;
        if($this->db->insert('text_friendlink',$data)){
            return true;
        }else return false;
    }

    public function modifyShow($id){
        $id=(int)$id;
        $sql="select `id`, `name`, `url`, `picurl` from `text_friendlink` where `id`=$id";
        $result=$this->db->getOne($sql);
        return $result;
    }

    public function modifySubmit($id,$name,$url,$picurl){
        $id=(int)$id;
        $data['name']=empty($name)?'':$name;
        $data['url']=empty($url)?'':$url;
        $data['picurl']=empty($picurl)?'':$picurl;
        if($this->db->modify('text_friendlink',$id,$data)){
            return true;
        }else return false;
    }

    public function delete($id){
        $id=(int)$id;
        if($this->db->hide('text_friendlink', $id)){
            return true;
        }else return false;
    }
} 