<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-4-30
 * Time: 下午7:53
 */

/**
 * Class mienMode
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class mienMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    public function getList($rows,$offset=0){
        $sql="select `id`, `title`, `picurl`,`create_time` from `text_mien`
        where `enable`=1 order by `create_time` DESC limit $offset,$rows";
        $result=$this->db->getAll($sql);
        return $result;
    }

    public function getRowsTotal(){
        $countSql="select count(1) from text_mien where `enable`=1";
        return $this->db->getValue($countSql);
    }

    public function createSubmit($title,$picurl){
        $data['title']=$title;
        if(!empty($picurl)) $data['picurl']=$picurl;
        if($this->db->insert('text_mien',$data)){
            return true;
        }else return false;
    }

    public function modifyShow($id){
        $id=(int)$id;
        $sql="select `id`, `title`, `picurl` from `text_mien` where `id`=$id";
        $result=$this->db->getOne($sql);
        return $result;
    }

    public function modifySubmit($id,$title,$picurl){
        $id=(int)$id;
        $data['title']=empty($title)?'':htmlentities($title,ENT_NOQUOTES,"utf-8");
        $data['picurl']=empty($picurl)?'':$picurl;
        if($this->db->modify('text_mien',$id,$data)){
            return true;
        }else return false;
    }

    public function delete($id){
        $id=(int)$id;
        if($this->db->hide('text_mien', $id)){
            return true;
        }else return false;
    }
    public function content($id){
        $id=(int)$id;
        $contentSql="select * from `text_mien` where `enable`=1 and `id`=$id";
        $result=$this->db->getOne($contentSql);
        return $result;
    }
} 