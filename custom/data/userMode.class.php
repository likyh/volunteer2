<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-5-1
 * Time: 上午9:02
 */

/**
 * Class userMode
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
import("Lib.Data.SimpleSession");
class userMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function activityList($id){
        $this->checkLogin();
        $id=(int)$id; //用户id
        $sql="select `a`.`activity_id`,`a`.`audit_time`,`c`.`name`,sum(`b`.`length`) as `totalLength`
              from `activity_user` as `a`,`activity_time` as `b`,`activity` as `c`
              where `a`.`userid`={$id}
              and `b`.`id`=`a`.`timeid`
              and `c`.`id`=`a`.`activity_id`
              and `c`.`id`=`b`.`activity_id`
              and `c`.`auth`=1
              and `a`.`checked`=1
              group by `a`.`activity_id`";
              //该用户每个活动的时长总和，1、该活动要审核通过，2、该活动时长用户审核通过（才能计算时长）
        $result['list']=$this->db->getAll($sql);
        $result['activitySum']=0;
        foreach($result['list'] as $v){
            $result['activitySum']+=$v['totalLength'];
        }
        return $result;
    }

    public function activityContent($activity_id,$userid){
        $this->checkLogin();
        $activity_id=(int)$activity_id; //活动id
        $userid=(int)$userid;           //用户id
        $activitySql="select `a`.`id`,`organizer`,`b`.`name` as `state`,`a`.`name`,`c`.`nickname` as `author`
                      from `activity` `a`,`activity_state` `b`,`admin_user` `c`
                      where `a`.`state_id`=`b`.`id`
                      and `a`.`typeid`=`c`.`admin_user_id`
                      and `a`.`id`=$activity_id";
        $result['activity']=$this->db->getOne($activitySql);
        $timeSql="select `a`.`timeid`,`a`.`checked`,`a`.`rejected`,`b`.`length`,`a`.`audit_time`,
                  `b`.`start`,`b`.`end`, `b`.`expired`,`b`.`state_id`
                  from `activity_user` as `a`,`activity_time` as `b`
                  where `a`.`timeid`=`b`.`id`
                  and `a`.`activity_id`=`b`.`activity_id`
                  and `a`.`userid`=$userid
                  and `a`.`activity_id`=$activity_id";
        $result['time']=$this->db->getAll($timeSql);
        $result['timeSum']=0;
        foreach ( $result['time'] as $v ){
            $result['timeSum']+=$v['checked']*$v['length'];//totalLength
        }
        return $result;
    }
    public function tmpRank($limit=100,$userId=0){
        $userId=(int)$userId;
        $limit=(int)$limit;
        $sql="select * from `rank_tmp` order by `rank` limit $limit";
        $result['rank']=$this->db->getAll($sql);
        if($userId!=0){
            foreach($result['rank'] as $v){
                if($v['user_id']==$userId){
                    $result['userRank']=$v['rank'];
                    $result['sum']=$v['sum'];
                }
            }
        }
        return $result;
    }
    public function userRank($limit=100,$userId=0){
        $userId=(int)$userId;
        $sql="select `userid`,sum(length) as `sum`,`c`.`name`
                from `activity_user` `a`,`activity_time` `b`,`studentinfo` `c`,`user_info` `d`
                where `a`.`timeid`=`b`.`id`
				and `c`.`num`=`d`.`num`
				and `d`.`id`=`a`.`userid`
                and `a`.`rejected`=0
                and `a`.`checked`=1
                group by `userid`
                order by `sum` DESC limit $limit";
        $result['rank']=$this->db->getAll($sql);
        $total=$this->db->getValue("select count(DISTINCT `userid`) from `activity_user`
                                    where `rejected`=0 and `checked`=1");
        for($i=0;$i<$total&&$i<$limit;$i++){
            $result['rank'][$i]['userRank']=$i+1;
        }
        if($userId!=0){
            foreach($result['rank'] as $v){
                if($v['userid']==$userId){
                    $result['userRank']=$v['userRank'];
                    $result['sum']=$v['sum'];
                }
            }
        }
        return $result;
    }

    public function alterPass($id,$num,$oldPass,$newPass){
        if($this->checkLogin()){
            $id=(int)$id;
            $oldPass=getPassWord($num, $oldPass);
            $old_pass=$this->db->getValue("select `password` from user_info where `num`={$num}");
            if($oldPass==$old_pass){
                $new_pass=getPassWord($num,$newPass); // 新密码
                $up_str='`password`='. $this->db->quote($new_pass);
                $sql="UPDATE  user_info  set  {$up_str}  where `id`=$id and `enable`='1' ";
                if($this->db->sqlExec($sql)){
                    return true;
                }else return false;
            }else return false;
        }else{
            header("Location:".getAppInfo()->getURL(array("user","login")));return;
        }
    }

    public function alterInfo($id,$team,$tel,$qq){
        if($this->checkLogin()){
            $id=(int)$id;
            $set= array();
            if(!empty($team)){
                $set['team']='`affiliations`='.$this->db->quote($team);
            }
            if(!empty($tel)){
                $set['tel']='`tel`='.$this->db->quote($tel);
            }
            if(!empty($qq)){
                $set['qq']='`qq`='.$this->db->quote($qq);
            }
            $sql="UPDATE  `user_info`  set ".implode(" , ", $set)." where `id`=$id and `enable`=1 " ;
            if(count($set)>0&& $this->db->sqlExec($sql)>0){
                return true;
            }else return false;
        }else{
            header("Location:".getAppInfo()->getURL(array("user","login")));return;
        }

    }
    public function login($num,$pass){
        if(!$this->checkLogin()){
            if(isset($num)&& !empty($num)&&isset($pass)&&!empty($pass)){
                $pass=$this->db->quote(getPassWord($num, $pass));
                $num=$this->db->quote($num);
                // 用用户名和密码进行查询
                $existSql="select `a`.`id`,`a`.`num`,`b`.`name` from `user_info` `a`,`studentinfo` `b`
                         where `a`.`num`={$num}
                         and `b`.`num`=`a`.`num` and `password`={$pass}";
                $result=$this->db->getOne($existSql);
                if($result){
                    SimpleSession::init();
                    $_SESSION['logined']="1";
                    $_SESSION['user_id']=$result['id'];
                    $_SESSION['user_info']=$result;
                    return true;
                }else{
                    return false;
                }
            }
        }else{
            header("Location:".getAppInfo()->getURL(array("user","index")));return;
        }
    }
    public function checkLogin(){
        if(isset($_SESSION['logined'])&&$_SESSION['logined']==1){
            return true;
        }else return false;
    }
    public function registerInfo($name,$num){
        if(empty($name)|| empty($num)){
            $result['message']=new DataMessage(DataMessage::STATE_ERROR,"信息不完整");
            return $result;
        }
        $name=$this->db->quote($name);
        $num=$this->db->quote($num);
        $sql="SELECT * FROM `studentinfo` where `name`={$name} and `num`={$num}
        and `registered`='0' limit 1";
        $result['register']=$this->db->getOne($sql);
        if(isset($result['register'])&&  !empty($result['register'])){
            // 检查该用户是否已经注册
            $existed="SELECT * FROM  user_info where `num`={$num} ;";
            $re=$this->db->getExist($existed);
            if($re){
                $result['message']=new DataMessage(DataMessage::STATE_ERROR,"该用户已注册");
            }else{
                $result['message']=new DataMessage(DataMessage::STATE_SUCCESS,"信息无误");
            }
        }else{
            $result['message']=new DataMessage(DataMessage::STATE_WARRING,"请核对您输入的信息");
        }
        return $result;
    }
    public function registerSubmit($password,$name,$num,$wechat,$affiliations,$tel,$qq,$love,$personal_text){
        if(empty($tel)|| empty($qq)|| empty($affiliations)){
            $result['message']=new DataMessage(DataMessage::STATE_WARRING,"信息不完整");
            return $result;
        }
        if(isset($password)&&!empty($password)){
            $name=$this->db->quote($name);
            $num=$this->db->quote($num);
            $data['password']=$password;
            $data['wechat']=htmlentities($wechat, ENT_NOQUOTES, "utf-8");
            $data['affiliations']=htmlentities($affiliations, ENT_NOQUOTES, "utf-8");
            $data['tel']=htmlentities($tel, ENT_NOQUOTES, "utf-8");
            $data['qq']=htmlentities($qq, ENT_NOQUOTES, "utf-8");
            $data['self_intro']=htmlentities($love. "<br/>\n". $personal_text);

            $ifRegistered="select count(*) from `user_info` where `num`={$num}";
            $ifR=$this->db->getValue($ifRegistered);//看有没有注册
            if($ifR<=0){
                $sql="select `num` from `studentinfo`
                       where `name`={$name} and `num`={$num} and `registered`='0' limit 1";
                $query=$this->db->getOne($sql);
                if($query){
                    // 该信息存在时注册
                    $data['num']=$query['num'];
                    $data['password']=getPassWord($data['num'], $data['password']);
                    $rs=$this->db->insert("user_info", $data);
                    if($rs> 0){
                        $sql="select `a`.`id`,`a`.`num`,`b`.`name` from `user_info` `a`,`studentinfo` `b`
                         where `a`.`num`={$data['num']}
                         and `b`.`num`=`a`.`num` Limit 1";
                        $query2=$this->db->getOne($sql);
                        SimpleSession::init();
                        $_SESSION['logined']="1";
                        $_SESSION['user_id']=$query2['id'];
                        $_SESSION['user_info']=$query2;
                        $result['message']=new DataMessage(DataMessage::STATE_SUCCESS,"注册成功");
                    }else{
                        $result['message']=new DataMessage(DataMessage::STATE_ERROR,"注册失败");
                    }
                }else{
                    $result['message']=new DataMessage(DataMessage::STATE_WARRING,"信息有误");
                }
            }else{
                $result['message']=new DataMessage(DataMessage::STATE_WARRING,"该用户已注册");
            }

        }else{
            $result['message']=new DataMessage(DataMessage::STATE_INFO,"密码为空");
        }
        return $result;
    }
    public function getInfo($id){
        $id=(int)$id;
        $sql="select `a`.`num`,`a`.`tel`,`a`.`qq`,`a`.`affiliations`,`b`.`name`,`b`.`academy`,`b`.`sex`,`b`.`class`
        from `user_info` as `a`,`studentinfo` as `b`
        where `a`.`id`={$id}
        and `a`.`num`=`b`.`num`";
        $result=$this->db->getOne($sql);
        return $result;
    }
    public function resetPass($username,$num){
        $sql="select `a`.`id` from `user_info` `a`,`studentinfo` `b`
                         where `a`.`num`={$this->db->quote($num)}
                         and `b`.`num`=`a`.`num`
                         and `b`.`name`={$this->db->quote($username)}";
        $id=$this->db->getValue($sql);
        if(!empty($id)){
            $data['password']=getPassWord($num,123456);
            $re=$this->db->modify('user_info',$id,$data);
            if($re){
                $result['state']="重置成功，密码为123456";
                $result['r']=1;
            }
        }else{
             $result['state']="您输入的信息错误，也许您还没有注册噢";
             $result['r']=0;
        }
        return $result;
    }
}