<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 14-4-29
 * Time: 下午10:03
 */

/**
 * Class noticeMode
 * ▲▲表示前后台通用的函数
 */
import("Lib.Data.SqlDB");
import('Lib.Core.Data');
class noticeMode extends Data {
    /**
     * @var SqlDB
     */
    protected $db;
    protected function onStart(){
        $this->db= SqlDB::init();
    }

    /**
     * 公告栏记录
     * @param int $rows
     * @param int $offset
     * @return mixed
     */
    public function getList($rows,$offset=0){
        $sql="select `id`, `content`, `create_time` from `text_notice`
        where `enable`=1 order by `create_time` desc limit $offset,$rows";
        $noticeList=$this->db->getAll($sql);
        return $noticeList;
    }
    public function getLatestNotice(){
        $sql="select `id`, `content` from `text_notice` where `enable`=1 order by `create_time` desc limit 1";
        $noticeList=$this->db->getOne($sql);
        return $noticeList;
    }
    public function createSubmit($content){
        $data['content']=empty($content)?'':htmlentities($content,ENT_NOQUOTES,"utf-8");
        if($this->db->insert('text_notice',$data)){
            return true;
        }else return false;
    }
} 