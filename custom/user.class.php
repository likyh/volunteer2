<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-5-17
 * Time: 上午9:44
 */
import('Lib.Core.Activity');
import("custom.data.userMode");
import("Lib.Data.SimpleSession");
import("Lib.core.WebRequestAnalysis");
class user extends Activity{
    /**
     * @var userMode
     */
    protected $article;
    protected $url;
    protected function onStart(){
        parent::onStart();
        $this->article=userMode::init();
        $this->url=WebRequestAnalysis::init();
        SimpleSession::init();
    }
    public function repassTask(){
        View::displayAsHtml(array(), "tpl/user/repass.php");
    }
    public function repassSubmitTask(){
        // TODO 增加重置密码
        $result=array();
        $result=$this->article->resetPass($_POST['username'],$_POST['num']);
        View::displayAsHtml($result, "tpl/user/repassSubmit.php");
    }
    public function registerTask(){
        View::displayAsHtml(array(), "tpl/user/register.php");
    }
    public function regInfoTask(){
        $result=$this->article->registerInfo($_POST['username'],$_POST['num']);
        $state=$result['message']->state;
        switch($state){
            case 0:View::displayAsHtml($result, "tpl/user/regInfo.php");break;
            case 1:case 2:case 3:default:
            $result['info']=$result['message']->title;
            View::displayAsHtml($result, "tpl/user/regSubmit.php");break;
        }
    }
    public function regSubmitTask(){
        $result=$this->article->registerSubmit($_POST['password'],$_POST['username'],$_POST['num'],
            $_POST['wechat'],$_POST['affiliations'],$_POST['tel'],$_POST['qq'],$_POST['love'],$_POST['personal_text']);
        $result['info']=$result['message']->title;
        View::displayAsHtml($result, "tpl/user/regSubmit.php");
    }
    public function loginTask(){
        View::displayAsHtml(array(), "tpl/user/login.php");
    }
    public function logedTask(){
        $result=$this->article->login($_POST['username'],$_POST['password']);
        if($result){
            header("Location:".$this->url->getURL(null,"user","index"));return;
        }else{
            echo "<meta charset='utf-8' />登录失败！点击<a href='javascript:window.history.go(-1);'>返回</a>重新登录，或者
                <a href='".$this->url->getURL(null,"user","repass")."'>点击</a>重置密码";
        }
    }
    public function indexTask(){
        if($this->article->checkLogin()){
            import("custom.data.activityMode");
            import("custom.data.downloadMode");
            $activity=activityMode::init();
            $download=downloadMode::init();
            $result['activity_catalog']=$activity->activityList(5);
            $result['download_catalog']=$download->getList(5);
            View::displayAsHtml($result, "tpl/user/index.php");
        }else{
            header("Location:".$this->url->getURL(null,"user","login"));return;
        }
    }
    public function logoutTask(){
        session_destroy();
        header("Location:".$this->url->getURL(null,"home","index"));return;

    }

    public function alterPassTask(){
        if($this->article->checkLogin()){
            $id=(int)$_SESSION['user_info']['id'];
            $num=$_SESSION['user_info']['num'];
            $result=array();
            if(isset($_POST['password'])&&isset($_POST['new_password1'])&&!empty($_POST['password'])&&!empty($_POST['new_password1'])){
                $r=$this->article->alterPass($id,$num,$_POST['password'],$_POST['new_password1']);
                if($r){
                    $result['msg']="修改成功";
                }else{
                    $result['msg']="修改失败";
                }
                echo $result['msg'];
            }
            View::displayAsHtml($result, "tpl/user/alterPass.php");
        }else{
            header("Location:".$this->url->getURL(null,"user","login"));return;
        }

    }
    public function alterInfoTask(){
        if($this->article->checkLogin()){
            $id=(int)$_SESSION['user_info']['id'];
            $result['user_info']=$this->article->getInfo($id);
            if(isset($_POST['submit'])){
                if(empty($_POST['team'])&&empty($_POST['tel'])&&empty($_POST['qq'])){
                    echo "修改失败"."您可能没有修改信息";
                } else{
                    $r=$this->article->alterInfo($id,$_POST['team'],$_POST['tel'],$_POST['qq']);
                    if($r){
                        echo "修改成功";header("Location:".$this->url->getURL(null,"user","alterInfo"));
                    }else{
                        echo "修改失败";header("Location:".$this->url->getURL(null,"user","alterInfo"));
                    }
                }
            }
            View::displayAsHtml($result, "tpl/user/alterInfo.php");
        }else{
            header("Location:".$this->url->getURL(null,"user","login"));return;
        }
    }
    public function myListTask(){
        if($this->article->checkLogin()){
            $id=(int)$_SESSION['user_info']['id'];
            $result=$this->article->activityList($id);
            View::displayAsHtml($result, "tpl/user/myList.php");
        }else{
            header("Location:".$this->url->getURL(null,"user","login"));return;
        }
    }
    public function myContentTask(){
        if($this->article->checkLogin()){
            $id=(int)$_SESSION['user_info']['id'];
            if(isset($_GET['id'])){
                $activity_id=$_GET['id'];
                $result=$this->article->activityContent($activity_id,$id);
                View::displayAsHtml($result, "tpl/user/myContent.php");
            }else{
                header("Location:".$this->url->getURL(null,"user","myList"));return;
            }
        }else{
            header("Location:".$this->url->getURL(null,"user","login"));return;
        }
    }
    public function introductionTask(){
        if($this->article->checkLogin()){
            View::displayAsHtml(array(), "tpl/user/introduction.php");
        }else{
            header("Location:".$this->url->getURL(null,"user","login"));return;
        }
    }
    public function rankTask(){
        if($this->article->checkLogin()){
            $id=(int)$_SESSION['user_info']['id'];
            $result=$this->article->tmpRank(50,$id);
            $result['username']=$_SESSION['user_info']['name'];
            View::displayAsHtml($result, "tpl/user/rank.php");
        }else{
            header("Location:".$this->url->getURL(null,"user","login"));return;
        }
    }
}