<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 14-5-15
 * Time: 下午8:55
 */
import('Lib.Core.Activity');
import("custom.data.activityMode");
import("custom.data.userMode");
import("Lib.Data.SimpleSession");
class act  extends  Activity{
    /**
     * @var activityMode
     */
    protected $activity;
    protected function onStart(){
        SimpleSession::init();
        parent::onStart();
        $this->activity=activityMode::init();
        $this->user=userMode::init();
    }
    function listTask(){
        $pageSize=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $date=isset($_GET['date'])&&!empty($_GET['date'])?$_GET['date']:'';
        $state=isset($_GET['state'])&&!empty($_GET['state'])?(int)$_GET['state']:'';
        $author=isset($_GET['author'])&&!empty($_GET['author'])?$_GET['author']:'';
        $s=isset($_GET['s'])&&!empty($_GET['s'])?$_GET['s']:'';
        $total=$this->activity->getRowsTotal($author,$state,$date,$s);
        $list=$this->activity->activityList($pageSize,($page-1)*$pageSize,$author,$state,$date,$s);
        $result=$this->getPageInfo($page, $pageSize, $total);
        $result['list']=$list;
        $result['s']=array();
        if(!empty($author)) $result['s']['author']=$author;
        if(!empty($state)) $result['s']['state']=$state;
        if(!empty($date)) $result['s']['date']=$date;
        $result['page']=$page;
        $result['total']=$total;
        View::displayAsHtml($result, 'tpl/act/list.php');
    }

    // 根据页码获取分页显示信息
    function getPageInfo($page, $pageSize, $total){
        $pageOffest=getConfig('page','showOffest','site');
        $totalPage=ceil($total/$pageSize);
        $result['currentPage']=$page;
        $result['pageArray']=array();
        for($i=($page-$pageOffest>0?$page-$pageOffest:1);
            $i<=($page+$pageOffest>$totalPage?$totalPage:$page+$pageOffest);
            $i++){
            $result['pageArray'][]=$i;
        }
        return $result;
    }
    function contentTask(){
        $id=(int)$_GET['id'];
        $result=$this->activity->activityContent($id);
        View::displayAsHtml($result, 'tpl/act/content.php');

    }
    function actApplyTask(){
        if($this->user->checkLogin()){
            $activity_id=(int)$_GET['activity_id'];
            $time_id=(int)$_GET['time_id'];
            $user_id=(int)$_SESSION['user_info']['id'];
            $result=$this->activity->apply($activity_id,$user_id,$time_id);
        }else{
            $result['state']='请登陆后再试';
        }
//        $result['url']=WebRequestAnalysis::init()->getURL(null,"home","index");
        View::displayAsHtml($result, 'tpl/act/actApply.php');
    }
} 