<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-5-3
 * Time: 下午3:17
 */
import("custom.data.newsPicMode");
import('Custom.View.textView');
class picnews extends Activity{
    /**
     * @var newsPicMode
     */
    protected $article;
    /**
     * @var textView
     */
    protected $tView;

    protected function onStart(){
        parent::onStart();
        $this->article=newsPicMode::init();
        $this->tView=textView::init();
        if($this->tView instanceof textView){
            $this->tView->nav=textView::NAV_NEWS;
        }
    }

    function contentTask(){
        $content=$this->article->content($_GET['id']);

        $this->tView->commonContentScene($content);
    }

    function newsModuleTask(){
        $list=$this->article->getList(7);
        $this->tView->picNewsPartScene($list);
    }
} 