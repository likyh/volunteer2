<?php
import("Lib.Data.SimpleSession");
import("custom.data.newsMode");
import("custom.data.newsPicMode");
import("custom.data.activityMode");
import("custom.data.userMode");
import("custom.data.groupMode");
import("custom.data.planMode");
import("custom.data.downloadMode");
import("custom.data.projectMode");
import("custom.data.mienMode");
import("custom.data.friendlinkMode");
class module extends Activity {
    /**
     * 以Task结尾的方法才是外界可以访问的，这是出于安全考虑，
     */
    function headerTask(){
        SimpleSession::init();
        import("Custom.Data.noticeMode");
        //公告部分
        $noticeMode=noticeMode::init();
        $result['notice']=$noticeMode->getLatestNotice();
        //用户名显示
        if(isset($_SESSION['logined'])&&!empty($_SESSION['logined'])){
            $result['username']=$_SESSION['user_info']['name'];
        }else{
            $result['username']=null;
        }
        View::displayAsHtml($result, "tpl/module/header.php");
	}

    function tailTask(){
        $result=array();
        View::displayAsHtml($result, "tpl/module/tail.php");
    }
    function iLoginTask(){
        $result=array();
        View::displayAsHtml($result, "tpl/module/iLogin.php");
    }
    function picShowTask(){
        $picShow=newsPicMode::init();
        $result=$picShow->getList(6);
        View::displayAsHtml($result, "tpl/module/picShow.php");
    }
    function iActivityTask(){
        $iActivity=activityMode::init();
        $result['activity']=$iActivity->activityList(6);
        View::displayAsHtml($result, "tpl/module/iActivity.php");
    }
    function rankTask(){
        $rank=userMode::init();
        $result=$rank->tmpRank(8);
        View::displayAsHtml($result, "tpl/module/rank.php");
    }
    function planTask(){
        $plan=planMode::init();
        $result=$plan->getList(3);
        View::displayAsHtml($result, "tpl/module/plan.php");
    }
    function railTask(){
        View::displayAsHtml(array(), "tpl/module/rail.php");
    }
    function downloadTask(){
        $download=downloadMode::init();
        $result=$download->getList(5);
        View::displayAsHtml($result, "tpl/module/download.php");
    }
    function iProjectTask(){
        $iProject=projectMode::init();
        $result=$iProject->getList(4);
        View::displayAsHtml($result, "tpl/module/iProject.php");
    }
    function mienTask(){
        $mien=mienMode::init();
        $result=$mien->getList(16);
        View::displayAsHtml($result, "tpl/module/mien.php");
    }
    function friendLinkTask(){
        $friendLink=friendlinkMode::init();
        $result=$friendLink->getList(10);
        View::displayAsHtml($result, "tpl/module/friendLink.php");
    }
    function sideActivityTask(){
        $sideActivity=activityMode::init();
        $result=$sideActivity->activityList(6);
        View::displayAsHtml($result, "tpl/module/sideActivity.php");
    }
    function projectSearchTask(){
        $projectSearch=projectMode::init();
        $result['type'] = $projectSearch->getType();
        $result['state']=$projectSearch->getState();
        View::displayAsHtml($result, "tpl/module/projectSearch.php");
    }
    function shareTask(){
        View::displayAsHtml(array(), "tpl/module/share.php");
    }
    function uTopNavTask(){
        import("custom.data.noticeMode");
        $notice=noticeMode::init();
        $result['notice']=$notice->getLatestNotice();
        import("custom.data.userMode");
        $user=userMode::init();
        $result['username']=$_SESSION['user_info']['name'];
        $userId=$_SESSION['user_info']['id'];
        $result['allRank']=$user->tmpRank(50,$userId);
        View::displayAsHtml($result, "tpl/module/uTopNav.php");
    }
    function uSideNavTask(){
        View::displayAsHtml(array(), "tpl/module/uSideNav.php");
    }
}
