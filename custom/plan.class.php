<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 14-5-3
 * Time: 下午2:58
 */
import('Lib.Core.Activity');
import("custom.data.planMode");
import('Custom.View.textView');
class plan extends Activity{
    /**
     * @var planMode
     */
    protected $article;
    protected function onStart(){
        parent::onStart();
        $this->article=planMode::init($this->appInfo);
    }
    function listTask(){
        $pageSize=getConfig('page','size','site');
        $total=$this->article->getRowsTotal();
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $list=$this->article->getList($pageSize,($page-1)*$pageSize);

        $view=textView::init($this->appInfo);
        if($view instanceof textView){
            $view->nav=textView::NAV_PLAN;
            $view->commonListScene($list, $page, $pageSize, $total);
        }
    }
    function contentTask(){
        $content=$this->article->content($_GET['id']);
        $view=textView::init($this->appInfo);
        if($view instanceof textView){
            $view->nav=textView::NAV_PLAN;
            $view->commonContentScene($content);
        }
    }

} 