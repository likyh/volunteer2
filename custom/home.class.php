<?php
import('Lib.Core.Activity');
import('Custom.View.textView');
import("Lib.Data.SimpleSession");
class home extends Activity{
    function indexTask(){
        SimpleSession::init();
        if(isset($_SESSION['logined'])&&!empty($_SESSION['logined'])){
            $result['user_logined']=$_SESSION['logined'];
        }else{
            $result['user_logined']=false;
        }
        //$result=array();
        View::displayAsHtml($result, 'tpl/index.php');
    }
} 