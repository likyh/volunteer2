<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 14-5-3
 * Time: 下午2:58
 */
import('Lib.Core.Activity');
import("custom.data.projectMode");
import('Custom.View.textView');
class project extends Activity{
    /**
     * @var projectMode
     */
    protected $article;
    protected function onStart(){
        parent::onStart();
        $this->article=projectMode::init($this->appInfo);
    }/*
    function listTask(){
        $pageSize=getConfig('page','size','site');
        $total=$this->article->getRowsTotal();
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $list=$this->article->getList($pageSize,($page-1)*$pageSize);

        $view=textView::init($this->appInfo);
        if($view instanceof textView){
            $view->nav=textView::NAV_PROJECT;
            $view->commonListScene($list, $page, $pageSize, $total);
        }*/
    /*
    function contentTask(){
        $content=$this->article->content($_GET['id']);
        $view=textView::init($this->appInfo);
        if($view instanceof textView){
            $view->nav=textView::NAV_PROJECT;
            $view->commonContentScene($content);
        }
    }*/
    function listTask(){
        $pageSize=getConfig('page','size','site');
        $total=$this->article->getRowsTotal();
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $total=$this->article->getRowsTotal();
        $list=$this->article->getList($pageSize,($page-1)*$pageSize);
        $result=$this->getPageInfo($page, $pageSize, $total);
        $result['list']=$list;
        View::displayAsHtml($result, "tpl/project/list.php");

    }
    // 根据页码获取分页显示信息
    function getPageInfo($page, $pageSize, $total){
        $pageOffest=getConfig('page','showOffest','site');
        $totalPage=ceil($total/$pageSize);
        $result['currentPage']=$page;
        $result['pageArray']=array();
        for($i=($page-$pageOffest>0?$page-$pageOffest:1);
            $i<=($page+$pageOffest>$totalPage?$totalPage:$page+$pageOffest);
            $i++){
            $result['pageArray'][]=$i;
        }
        return $result;
    }
    function contentTask(){
        $result=$this->article->content($_GET['id']);
        View::displayAsHtml($result, "tpl/project/content.php");
    }
} 