<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-5-3
 * Time: 下午1:00
 */
import('Lib.Core.Activity');
import("custom.data.groupMode");
import('Custom.View.textView');
class group extends Activity {
    /**
     * @var groupMode
     */
    protected $article;
    protected function onStart(){
        parent::onStart();
        $this->article=groupMode::init($this->appInfo);
    }
    function listTask(){
        $pageSize=getConfig('page','size','site');
        $total=$this->article->getRowsTotal();
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $list=$this->article->getList($pageSize,($page-1)*$pageSize);

        $view=textView::init($this->appInfo);
        if($view instanceof textView){
            $view->nav=textView::NAV_GROUP;
            $view->commonListScene($list, $page, $pageSize, $total);
        }
    }
    function contentTask(){
        $content=$this->article->content($_GET['id']);
        $view=textView::init($this->appInfo);
        if($view instanceof textView){
            $view->nav=textView::NAV_GROUP;
            $view->commonContentScene($content);
        }
    }

    function moduleTask(){
        $result=$this->article->getList(7);
        View::displayAsHtml($result, "tpl/module/group.php");
    }
}
