<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-5-3
 * Time: 下午12:35
 */
import('Lib.Core.View');
class textView extends View{
    const NAV_NEWS=0;
    const NAV_ACTIVITY=1;
    const NAV_PLAN=2;
    const NAV_GROUP=3;
    const NAV_FRIENDLINK=4;
    const NAV_PROJECT=5;
    const NAV_DOWNLOAD=6;

    public $nav;

    // 根据页码获取分页显示信息
    function getPageInfo($page, $pageSize, $total){
        $pageOffest=getConfig('page','showOffest','site');
        $totalPage=ceil($total/$pageSize);
        $result['currentPage']=$page;
        $result['pageArray']=array();
        for($i=($page-$pageOffest>0?$page-$pageOffest:1);
            $i<=($page+$pageOffest>$totalPage?$totalPage:$page+$pageOffest);
            $i++){
            $result['pageArray'][]=$i;
        }
        return $result;
    }

    //没有搜索和选择的列表
    function commonListScene($list, $page, $pageSize, $total){
        $result=$this->getPageInfo($page, $pageSize, $total);
        list($result['pageTitle'], $result['link'])=$this->checkNav();
        $result['list']=$list;
        $result['page']=$page;
        $this->displayAsHtml($result, "tpl/commonList.php");
    }

    // 新闻列表
    function newsListScene($list, $page, $pageSize, $total, $type=null,$s=null){
        $result=$this->getPageInfo($page, $pageSize, $total);
        $result['list']=$list;
        list($result['pageTitle'], $result['link'])=$this->checkNav();
        $result['page']=$page;
        $result['type']=$type;
        $result['s']=$s;

        $this->displayAsHtml($result, "tpl/news/list.php");
    }

    function commonContentScene($content){
        list($content['pageTitle'], $content['link'])=$this->checkNav();
        $this->displayAsHtml($content, "tpl/commonContent.php");
    }

    function commonPartScene($list){
        list($result['title'], $result['link'], $result['name'])=$this->checkNav();
        $result['list']=$list;
        $this->displayAsHtml($result, "tpl/commonPart.php");
    }

    // 图片新闻模块
    function picNewsPartScene($list){
        $result['list']=$list;
        $this->displayAsHtml($result, "tpl/news/picNewsPart.php");
    }

    function checkNav(){
        switch($this->nav){
            case self::NAV_ACTIVITY:
                $pageTitle="公益活动报名";
                $link="公益活动报名";
                $name="activity";
                break;
            case self::NAV_PLAN:
                $pageTitle="专项计划";
                $link="专项计划";
                $name="plan";
                break;
            case self::NAV_GROUP:
                $pageTitle="公益团体";
                $link="公益团体";
                $name="group";
                break;
            case self::NAV_PROJECT:
                $pageTitle="志愿项目展示";
                $link="志愿项目展示";
                $name="project";
                break;
            case self::NAV_DOWNLOAD:
                $pageTitle="相关下载";
                $link="相关下载";
                $name="download";
                break;
            default:
            case self::NAV_NEWS:
                $pageTitle="新闻资讯";
                $link="新闻资讯";
                $name="news";
                break;
        }
        return array($pageTitle,$link, $name);
    }
} 