<?php
class FormWidget implements ArrayAccess,Countable {
    public $action;
    public $method;
    public $enctype;
    public $classArray=array();
    public $fieldArray=array();
    public $autoAddSubmit=true;

    function __construct($action, $method=null, $enctype=null) {
        $this->action = $action?: "#";
        $this->method = $method?: "POST";
        $this->enctype = $enctype?: "multipart/form-data";
    }
    public function addClass($class) {
        $this->classArray[] = $class;
    }

    /**
     * name字段用于标示表单组的用途，比如提交按钮就可以放在submit组里面
     * @param string $name
     * @param string $title
     */
    public function createAddFieldSet($name, $title){
        $this->fieldArray[$name]=new FieldSetWidget($name, $title);
    }
    /**
     * @param FieldSetWidget $FieldSet
     */
    public function addFieldSet($FieldSet){
        $this->fieldArray[$FieldSet->name]=$FieldSet;
    }

    public function offsetExists($offset) {
        return isset($this->fieldArray[$offset]);
    }

    /**
     * @param mixed $offset
     * @return FieldSetWidget
     */
    public function offsetGet($offset) {
        return $this->fieldArray[$offset];
    }

    /**
     * @param mixed $offset
     * @param FieldSetWidget $value
     */
    public function offsetSet($offset, $value) {
        $this->fieldArray[$offset]=$value;
    }
    public function offsetUnset($offset) {
        unset($this->fieldArray[$offset]);
    }
    public function count() {
        return count($this->fieldArray);
    }

    public function autoAddSubmit(){
        if($this->autoAddSubmit&& !isset($this['submit'])){
            $this->createAddFieldSet("submit","填写完成");
            $this["submit"]->createAddInput(null, "提交", InputWidget::TYPE_SUBMIT);
        }
    }

    function __toString() {
        $this->autoAddSubmit();
        $html="<form action='{$this->action}' method='{$this->method}'>";
        foreach($this->fieldArray as $v){
            $html.=$v;
        }
        $html.="</form>";
        return $html;
    }
}
class FieldSetWidget implements ArrayAccess,Countable {
    public $inputArray;
    public $title;
    public $name;

    function __construct($name, $title) {
        $this->name = $name;
        $this->title = $title;
    }

    public function createAddInput($name, $title, $type, $tips=null, $value=""){
        $this->inputArray[]=new InputWidget($name, $title, $type, $tips, $value);
    }

    /**
     * @param InputWidget $input
     */
    public function addInput($input){
        $this->inputArray[]=$input;
    }

    public function offsetExists($offset) {
        return isset($this->inputArray[$offset]);
    }

    /**
     * @param mixed $offset
     * @return InputWidget
     */
    public function offsetGet($offset) {
        return $this->inputArray[$offset];
    }

    /**
     * @param mixed $offset
     * @param InputWidget $value
     */
    public function offsetSet($offset, $value) {
        $this->inputArray[$offset]=$value;
    }
    public function offsetUnset($offset) {
        unset($this->inputArray[$offset]);
    }
    public function count() {
        return count($this->inputArray);
    }

    function __toString() {
        $html="<fieldset class='{$this->name}'><legend>{$this->title}</legend>";
        foreach($this->inputArray as $v){
            $html.=$v;
        }
        $html.="</fieldset>";
        return $html;
    }

}
class InputWidget {
    const TYPE_TEXT=0;
    const TYPE_EMAIL=1;
    const TYPE_TEL=13;
    const TYPE_COLOR=14;
    const TYPE_URL=2;
    const TYPE_PASSWORD=3;
    const TYPE_HIDDEN=4;
    const TYPE_FILE=5;
    const TYPE_CHECKBOX=6;
    const TYPE_RADIO=7;
    const TYPE_SELECT=8;
    const TYPE_TEXTAREA=9;
    const TYPE_SUBMIT=10;
    const TYPE_RESET=11;
    const TYPE_BUTTON=12;

    const TYPE_TIME=32;
    const TYPE_ADDRESS=33;

    public $name;
    public $title;
    public $type;
    public $tips;
    public $value;
    public $changeAble=true;//TODO 未实现
    public $required=false;
    public $patten;
    public $optionArray;

    function __construct($name, $title, $type, $tips=null, $value="") {
        $this->name = $name;
        $this->title = $title;
        $this->type = $type;
        $this->tips = $tips;
        $this->value = $value;
    }


    public function addOption($title, $value) {
        $this->optionArray[$title] = $value;
    }
    public function addOptionArray($array) {
        array_merge($this->optionArray, $array);
    }

    function __toString() {
        $name = $this->name;
        $title = $this->title;
        $type = $this->type;
        $tips = $this->tips;
        $value = $this->value;
        $html="";
        switch($type){
            case self::TYPE_TEXT:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='text' name='{$name}' id='{$name}Input' placeholder='{$tips}' value='{$value}'/>";
                break;
            case self::TYPE_EMAIL:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='email' name='{$name}' id='{$name}Input' placeholder='{$tips}' value='{$value}'/>";
                break;
            case self::TYPE_TEL:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='tel' name='{$name}' id='{$name}Input' placeholder='{$tips}' value='{$value}'/>";
                break;
            case self::TYPE_COLOR:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='color' name='{$name}' id='{$name}Input' placeholder='{$tips}' value='{$value}'/>";
                break;
            case self::TYPE_URL:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='url' name='{$name}' id='{$name}Input' placeholder='{$tips}' value='{$value}'/>";
                break;
            case self::TYPE_PASSWORD:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='password' name='{$name}' id='{$name}Input' placeholder='{$tips}' value='{$value}'/>";
                break;
            case self::TYPE_HIDDEN:
                $html="<input type='hidden' name='{$name}' id='{$name}Input' value='{$value}'/>";
                break;
            case self::TYPE_FILE:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='file' name='{$name}' id='{$name}Input' placeholder='{$tips}'/>";
                break;
            case self::TYPE_CHECKBOX:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='checkbox' name='{$name}' id='{$name}Input'/>";
                break;
            case self::TYPE_RADIO:
                //TODO
                $rand=mt_rand(0,1000);
                $html="<label for='{$name}Input{$rand}'>{$title}</label>
            <input type='radio' name='{$name}' id='{$name}Input{$rand}'/>";
                break;
            case self::TYPE_SELECT:
                //TODO
                $html="<label for='{$name}Input'>{$title}</label>";
                $html.="<select name='{$name}' id='{$name}Input'>";
                foreach($this->optionArray as $k=>$v){
                    $html.="<option value='$k'>$v</option>";
                }
                $html.="</select>";
                break;
            case self::TYPE_TEXTAREA:
                //TODO
                $html="<label for='{$name}Input'>{$title}</label>
            <textarea name='{$name}' id='{$name}Input'>$value</textarea>";
                break;
            case self::TYPE_SUBMIT:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='submit' name='{$name}' id='{$name}Input'/>";
                break;
            case self::TYPE_RESET:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='reset' name='{$name}' id='{$name}Input'/>";
                break;
            case self::TYPE_BUTTON:
                $html="<label for='{$name}Input'>{$title}</label>
            <button name='{$name}' id='{$name}Input'>{$value}</button>";
                break;

            case self::TYPE_TIME:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='text' class='timeInput' name='{$name}' id='{$name}Input' data-tips='{$tips}' value='{$value}'/>";
                break;
            case self::TYPE_ADDRESS:
                $html="<label for='{$name}Input'>{$title}</label>
            <input type='text' class='addressInput' name='{$name}' id='{$name}Input' data-tips='{$tips}' value='{$value}'/>";
                break;
        }
        return $html;
    }


}