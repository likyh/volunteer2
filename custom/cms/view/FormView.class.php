<?php
class FormView extends View{
    const NAV_NEWS=0;
    const NAV_ACTIVITY=1;
    const NAV_PLAN=2;
    const NAV_GROUP=3;
    const NAV_FRIENDLINK=4;
    const NAV_PROJECT=5;
    const NAV_DOWNLOAD=6;

    public $nav;

    /**
     * @param FormWidget $form
     */
    function formScene($form){
        $this->displayAsHtml(array("form"=>$form), "cms/tpl/form.php");
    }
}