<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-5-3
 * Time: 下午3:13
 */
import('Lib.Core.Activity');
import("custom.data.downloadMode");
import('Custom.View.textView');
class download extends Activity  {
    /**
     * @var downloadMode
     */
    protected $article;
    protected function onStart(){
        parent::onStart();
        $this->article=downloadMode::init($this->appInfo);
    }
    function listTask(){
        $pageSize=getConfig('page','size','site');
        $total=$this->article->getRowsTotal();
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $list=$this->article->getList($pageSize,($page-1)*$pageSize);

        $view=textView::init($this->appInfo);
        if($view instanceof textView){
            $view->nav=textView::NAV_DOWNLOAD;
            $view->commonListScene($list, $page, $pageSize, $total);
        }
    }
    function contentTask(){
        $content=$this->article->content($_GET['id']);
        $view=textView::init($this->appInfo);
        if($view instanceof textView){
            $view->nav=textView::NAV_PLAN;
            $view->commonContentScene($content);
        }
    }

    function moduleTask(){
        $result=$this->article->getList(7);
        View::displayAsHtml($result, "tpl/module/download.php");
    }
} 