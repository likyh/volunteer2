<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-5-3
 * Time: 下午3:17
 */
import('Lib.Core.Activity');
import("custom.data.newsMode");
import('Custom.View.textView');
import("custom.data.newsPicMode");
class news extends Activity{
    /**
     * @var newsMode
     */
    protected $article;
    /**
     * @var textView
     */
    protected $tView;

    protected function onStart(){
        parent::onStart();
        $this->article=newsMode::init();
        $this->tView=textView::init();
        if($this->tView instanceof textView){
            $this->tView->nav=textView::NAV_NEWS;
        }
    }

    function listTask(){
        $pageSize=getConfig('page','size','site');
        $total=$this->article->getRowsTotal();
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $type=isset($_GET['type'])?$_GET['type']:'all';
        $s=isset($_GET['q'])?$_GET['q']:'';
        $list=$this->article->getList($pageSize,($page-1)*$pageSize, $type,$s);

        $this->tView->newsListScene($list, $page, $pageSize, $total, $type,$s);
    }

    function contentTask(){
        $content=$this->article->content($_GET['id']);

        $this->tView->commonContentScene($content);
    }

    function sideNewsTask(){
        $list=$this->article->getList(7);
        $this->tView->commonPartScene($list);
    }

    function newsModuleTask(){
        $result=$this->article->getList(7);
        View::displayAsHtml($result, "tpl/news/iNews.php");
    }
    function content_picTask(){
        $newsPic=newsPicMode::init();
        $id=isset($_GET['id'])?(int)$_GET['id']:1;
        $result=$newsPic->content($id);
        View::displayAsHtml($result, "tpl/news/content_pic.php");
    }

}