<?php
import("custom.data.planMode");
import("Custom.Admin.adminBase");
import("lib.file.Uploader");
import("lib.common.Page");
class plan extends adminBase{
    /** @var planMode */
    protected $article;
    protected $up;
    protected function onStart(){
        parent::onStart();
        $this->article=planMode::init();
        $this->up=Uploader::init();
        $this->cms->setControlFile("admin/tpl/plan/control.json");
        $this->cms->setPageTitle("专项计划管理");
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
    }
    function showTask(){
        $this->cms->setActionTitle("所有专项计划");
        $page_size=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $plan_begin=($page-1)*$page_size;
        $total=$this->article->getRowsTotal();
        $result['plan']=$this->article->getList($page_size,$plan_begin);
        $url=WebRequestAnalysis::init()->getQuestion("page=");
        $result['page']=new Page($page,$total,$page_size);
        $result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
        $result['type_id']=$this->admin->getTypeId();
        $result['total']=$total;
        $this->cms->tableScene($result,"admin/tpl/plan/show.php");
    }
    function createTask(){
        $this->cms->setActionTitle("添加专项计划");
        $this->cms->normalScene(array(),"admin/tpl/plan/create.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function createSubmitTask(){
        $name=$_POST['name'];
        $organizer=$_POST['organizer'];
        $time=$_POST['time'];
        $place=$_POST['place'];
        $content=$_POST['content'];
        $description=$_POST['description'];
        list($ret,$upInfo)=$this->up->upFile("picurl");
        if(strtolower($ret)!="success"){
            $result[0]="图片上传失败";
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $re=$this->article->createSubmit($name,$content,$description,$time,$place,$organizer,$picurl);
        if($re){
            $result[0]="创建成功";
        }else{
            $result[0]="创建失败（注意专项计划不能重名）";
        }
        $this->cms->tableScene($result,"admin/tpl/plan/createSubmit.php");
    }
    function modifyTask(){
        $this->cms->setActionTitle("专项计划修改");
        $id=(int)$_GET['id'];
        $result=$this->article->content($id);
        $result['id']=$id;
        $this->cms->normalScene($result,"admin/tpl/plan/modify.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function modifySubmitTask(){
        $id=(int)$_POST['id'];
        $name=$_POST['name'];
        $organizer=$_POST['organizer'];
        $time=$_POST['time'];
        $place=$_POST['place'];
        $content=$_POST['content'];
        $description=$_POST['description'];
        list($ret,$upInfo)=$this->up->upFile("picurl");
        if(strtolower($ret)!="success"){
            $result[0]="图片上传失败";
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $re=$this->article->modifySubmit($id,$name,$content,$description,$time,$place,$organizer,$picurl);
        if($re){
            $result[0]="修改成功";
        }else{
            $result[0]="修改失败";
        }
        $this->cms->tableScene($result,"admin/tpl/plan/modifySubmit.php");
    }
    function deleteTask(){
        $id=(int)$_GET['id'];
        $re=$this->article->delete($id);
        if($re){
            $result[0]="删除成功";
        }else{
            $result[0]="删除失败";
        }
        $this->cms->tableScene($result,"admin/tpl/plan/delete.php");
    }
} 