<?php
import("custom.data.downloadMode");
import("Custom.Admin.adminBase");
import("lib.file.Uploader");
import("lib.common.Page");
class download extends adminBase {
    /** @var downloadMode */
    protected $article;
    protected $up;
    protected function onStart(){
        parent::onStart();
        $this->article=downloadMode::init();
        $this->up=Uploader::init();
        $this->cms->setControlFile("admin/tpl/download/control.json");
        $this->cms->setPageTitle("下载管理");
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
    }
    function showTask(){
        $this->cms->setActionTitle("所有下载资料");
        $page_size=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $down_begin=($page-1)*$page_size;
        $total=$this->article->getRowsTotal();
        $result['download']=$this->article->getList($page_size,$down_begin);
        $url=WebRequestAnalysis::init()->getQuestion("page=");
        $result['page']=new Page($page,$total,$page_size);
        $result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
        $result['type_id']=$this->admin->getTypeId();
        $result['total']=$total;
        $this->cms->tableScene($result,"admin/tpl/download/show.php");
    }
    function createTask(){
        $this->cms->setActionTitle("添加下载资料");
        $this->cms->normalScene(array(),"admin/tpl/download/create.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function createSubmitTask(){
        $title=$_POST['title'];
        list($ret,$upInfo)=$this->up->upFile("url");
        if(strtolower($ret)!="success"){
            $result[0]="资料上传失败";
            $url=null;
        }else{
            $url=$upInfo['url'];
        }
        $re=$this->article->createSubmit($title,$url);
        if($re){
            $result[0]="添加成功";
        }else{
            $result[0]="添加失败";
        }
        $this->cms->tableScene($result,"admin/tpl/download/createSubmit.php");
    }
    function modifyTask(){
        $this->cms->setActionTitle("下载资料内容修改");
        $id=(int)$_GET['id'];
        $result=$this->article->modifyShow($id);
        $result['id']=$id;
        $this->cms->normalScene($result,"admin/tpl/download/modify.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function modifySubmitTask(){
        $id=(int)$_POST['id'];
        $title=$_POST['title'];
        list($ret,$upInfo)=$this->up->upFile("url");
        if(strtolower($ret)!="success"){
            $result[0]="资料上传失败";
            $url=null;
        }else{
            $url=$upInfo['url'];
        }
        $re=$this->article->modifySubmit($id,$title,$url);
        if($re){
            $result[0]="修改成功";
        }else{
            $result[0]="修改失败";
        }
        $this->cms->tableScene($result,"admin/tpl/download/modifySubmit.php");
    }
    function deleteTask(){
        $id=(int)$_GET['id'];
        $re=$this->article->delete($id);
        if($re){
            $result[0]="删除成功";
        }else{
            $result[0]="删除失败";
        }
        $this->cms->tableScene($result,"admin/tpl/download/delete.php");
    }
} 