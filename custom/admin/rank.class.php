<?php
import("custom.data.userMode");
import("Custom.Admin.adminBase");
import("lib.common.Page");
class rank extends adminBase{
    /** @var newsMode */
    protected $cms;
    /** @var  adminMode */
    protected function onStart(){
        parent::onStart();
        $this->cms=CmsView::init();
        $this->user=userMode::init();
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
    }
    function updateTask(){
        $this->cms->setActionTitle("更新排名");
        $db=SqlDB::init();
        if(isset($_POST['update'])){
            $realRank=$this->user->userRank(50);
            $db->sqlExec("TRUNCATE TABLE rank_tmp");
            foreach ($realRank['rank'] as $v) {
                $data['user_id']=$v['userid'];
                $data['sum']=$v['sum'];
                $data['name']=$v['name'];
                $data['rank']=$v['userRank'];
                $db->insert('rank_tmp',$data);
            }
        }
        
        $sql="select * from `rank_tmp` order by `rank`";
        $result['rank']=$db->getAll($sql);
        $this->cms->tableScene($result,"admin/tpl/rank/update.php");
    }

} 