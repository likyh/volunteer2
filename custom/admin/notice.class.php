<?php
import("custom.data.noticeMode");
import("Custom.Admin.adminBase");
class notice extends adminBase{
    /** @var noticeMode */
    protected $article;
    protected function onStart(){
        parent::onStart();
        $this->article=noticeMode::init();
        $this->cms->setControlFile("admin/tpl/notice/control.json");
        $this->cms->setPageTitle("公告栏管理");
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
    }
    function showTask(){
        $this->cms->setActionTitle("历史公告");
        $result['notice']=$this->article->getList(10,0);
        $this->cms->tableScene($result,"admin/tpl/notice/show.php");
    }
    function createTask(){
        $this->cms->setActionTitle("添加公告");
        $this->cms->formScene(array(),"admin/tpl/notice/create.php");
    }
    function createSubmitTask(){
        $content=$_POST['content'];
        $re=$this->article->createSubmit($content);
        if($re){
            $result[0]="添加成功";
        }else{
            $result[0]="添加失败";
        }
        $this->cms->tableScene($result,"admin/tpl/notice/createSubmit.php");
    }
} 