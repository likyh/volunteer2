<?php
import("custom.data.mienMode");
import("Custom.Admin.adminBase");
import("lib.file.Uploader");
import("lib.common.Page");
class mien extends adminBase {
    /** @var mienMode */
    protected $article;
    protected $up;
    protected function onStart(){
        parent::onStart();
        $this->article=mienMode::init();
        $this->up=Uploader::init();
        $this->cms->setControlFile("admin/tpl/mien/control.json");
        $this->cms->setPageTitle("志愿者风采管理");
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
    }
    function showTask(){
        $this->cms->setActionTitle("所有志愿者");
        $page_size=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $mien_begin=($page-1)*$page_size;
        $total=$this->article->getRowsTotal();
        $result['mien']=$this->article->getList($page_size,$mien_begin);
        $url=WebRequestAnalysis::init()->getQuestion("page=");
        $result['page']=new Page($page,$total,$page_size);
        $result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
        $result['type_id']=$this->admin->getTypeId();
        $result['total']=$total;
        $this->cms->tableScene($result,"admin/tpl/mien/show.php");
    }
    function createTask(){
        $this->cms->setActionTitle("添加志愿者图片");
        $this->cms->normalScene(array(),"admin/tpl/mien/create.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function createSubmitTask(){
        $title=$_POST['title'];
        list($ret,$upInfo)=$this->up->upFile("picurl");
        if(strtolower($ret)!="success"){
            $result[0]="图片上传失败";
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $re=$this->article->createSubmit($title,$picurl);
        if($re){
            $result[0]="添加成功";
        }else{
            $result[0]="添加失败";
        }
        $this->cms->tableScene($result,"admin/tpl/mien/createSubmit.php");
    }
    function modifyTask(){
        $this->cms->setActionTitle("志愿者内容修改");
        $id=(int)$_GET['id'];
        $result=$this->article->content($id);
        $result['id']=$id;
        $this->cms->normalScene($result,"admin/tpl/mien/modify.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function modifySubmitTask(){
        $id=(int)$_POST['id'];
        $title=$_POST['title'];
        list($ret,$upInfo)=$this->up->upFile("picurl");
        if(strtolower($ret)!="success"){
            $result[0]="图片上传失败";
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $re=$this->article->modifySubmit($id,$title,$picurl);
        if($re){
            $result[0]="修改成功";
        }else{
            $result[0]="修改失败";
        }
        $this->cms->tableScene($result,"admin/tpl/mien/modifySubmit.php");
    }
    function deleteTask(){
        $id=(int)$_GET['id'];
        $re=$this->article->delete($id);
        if($re){
            $result[0]="删除成功";
        }else{
            $result[0]="删除失败";
        }
        $this->cms->tableScene($result,"admin/tpl/mien/delete.php");
    }
} 