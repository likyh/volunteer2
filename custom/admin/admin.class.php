<?php
/**
 * Created by PhpStorm.
 * User: sg
 * Date: 14-5-3
 * Time: 下午1:00
 */
import('Plugin.Cms.CmsView');
import("custom.data.adminMode");
class admin extends Activity {
    /** @var CmsView */
    protected $cms;
    /** @var  adminMode */
    protected $admin;
    protected function onStart(){
        parent::onStart();
        $this->admin=adminMode::init();
        $this->cms=CmsView::init();
    }
    function loginTask(){
        $web=WebRequestAnalysis::init();
        $this->cms->loginScene($web->getAction("loginSubmit"));
    }

    function loginSubmitTask(){
        $user=isset($_POST['user'])?$_POST['user']:null;
        $pass=isset($_POST['pass'])?$_POST['pass']:null;

        if($this->admin->login($user, $pass)){
            $web=WebRequestAnalysis::init();
            header("Location:".$web->getPage("dash","index"));
            exit();
        }else{
            echo "登录失败";
        }
    }

    function loginoutTask(){
        $web=WebRequestAnalysis::init();
        header("Location:".$web->getPage("admin","login"));
    }
}