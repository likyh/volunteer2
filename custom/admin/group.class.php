<?php
import("custom.data.groupMode");
import("Custom.Admin.adminBase");
import("lib.file.Uploader");
import("lib.common.Page");
class group extends adminBase {
    /** @var groupMode */
    protected $article;
    protected $up;
    protected function onStart(){
        parent::onStart();
        $this->article=groupMode::init();
        $this->up=Uploader::init();
        $this->cms->setControlFile("admin/tpl/group/control.json");
        $this->cms->setPageTitle("公益团体管理");
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
    }
    function showTask(){
        $this->cms->setActionTitle("所有公益团体");
        $page_size=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $group_begin=($page-1)*$page_size;
        $total=$this->article->getRowsTotal();
        $result['group']=$this->article->getList($page_size,$group_begin);
        $url=WebRequestAnalysis::init()->getQuestion("page=");
        $result['page']=new Page($page,$total,$page_size);
        $result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
        $result['type_id']=$this->admin->getTypeId();
        $result['total']=$total;
        $this->cms->tableScene($result,"admin/tpl/group/show.php");
    }
    function createTask(){
        $this->cms->setActionTitle("添加公益团体");
        $this->cms->normalScene(array(),"admin/tpl/group/create.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function createSubmitTask(){
        $title=$_POST['title'];
        $content=$_POST['content'];

        $re=$this->article->createSubmit($title,$content);
        if($re){
            $result[0]="创建成功";
        }else{
            $result[0]="创建失败（注意公益组织不能重名）";
        }
        $this->cms->tableScene($result,"admin/tpl/group/createSubmit.php");
    }
    function modifyTask(){
        $this->cms->setActionTitle("公益组织修改");
        $id=(int)$_GET['id'];
        $result=$this->article->content($id);
        $result['id']=$id;
        $this->cms->normalScene($result,"admin/tpl/group/modify.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function modifySubmitTask(){
        $id=(int)$_POST['id'];
        $title=$_POST['title'];
        $content=$_POST['content'];
        $re=$this->article->modifySubmit($id,$title,$content);
        if($re){
            $result[0]="修改成功";
        }else{
            $result[0]="修改失败";
        }
        $this->cms->tableScene($result,"admin/tpl/group/modifySubmit.php");
    }
    function deleteTask(){
        $id=(int)$_GET['id'];
        $re=$this->article->delete($id);
        if($re){
            $result[0]="删除成功";
        }else{
            $result[0]="删除失败";
        }
        $this->cms->tableScene($result,"admin/tpl/group/delete.php");
    }
} 