<?php
import('Plugin.Cms.CmsView');
import('Custom.Data.adminMode');
class adminBase extends Activity {
    /** @var CmsView */
    protected $cms;
    /** @var  adminMode */
    protected $admin;
    protected function onStart() {
        parent::onStart();
        $this->admin=adminMode::init();
        $this->cms=CmsView::init();
        $this->checkLogin();
    }

    function checkLogin(){
        if(!$this->admin->checkLogin()){
            $web=WebRequestAnalysis::init();
            header("Location:".$web->getPage("admin","login"));
            exit();
        }
    }

    public function getName(){
        return $this->admin->checkLogin()?$_SESSION['admin']['user']['user']:null;
    }



}