<?php
import("custom.data.newsMode");
import("Custom.Admin.adminBase");
import("lib.common.Page");
class news extends adminBase{
    /** @var newsMode */
    protected $article;
    protected function onStart(){
        parent::onStart();
        $this->article=newsMode::init();
        $this->cms->setControlFile("admin/tpl/news/control.json");
        $this->cms->setPageTitle("新闻管理");
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
    }
    function createTask(){
        $this->cms->setActionTitle("添加新闻");
        $result['author']=$this->admin->getCName();
        $this->cms->normalScene($result,"admin/tpl/news/create.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function createSubmitTask(){
        if(isset($_POST['title'])&&!empty($_POST['title'])&&isset($_POST['author'])&&!empty($_POST['author'])&&isset($_POST['date'])&&!empty($_POST['date'])&&isset($_POST['content'])&&!empty($_POST['content'])){
            $date=date("Y-m-d",strtotime($_POST['date']));
            if($date=="1970-01-01"){
                echo "<meta charset='utf-8'/><script>alert('时间格式填写有误！');window.history.back();</script>";
            }else{
                $title=$_POST['title'];
                $author=$_POST['author'];
                $date=$_POST['date'];
                $typeid=$this->admin->getTypeId();
                $content=$_POST['content'];
                $re=$this->article->createSubmit($title,$author,$date,$typeid,$content);
                if($re){
                    $result[0]="添加成功";
                }else{
                    $result[0]="添加失败";
                }
                $this->cms->tableScene($result,"admin/tpl/news/createSubmit.php");
            }
        }else{
            echo "<meta charset='utf-8'/><script>alert('信息填写不完整！');window.history.back();</script>";
        }
//        $title=$_POST['title'];
//        $author=$_POST['author'];
//        $date=$_POST['date'];
//        $typeid=$this->admin->getTypeId();
//        $content=$_POST['content'];
//        $re=$this->article->createSubmit($title,$author,$date,$typeid,$content);
//        if($re){
//            $result[0]="添加成功";
//        }else{
//            $result[0]="添加失败";
//        }
//        $this->cms->tableScene($result,"admin/tpl/news/createSubmit.php");
    }
    function showTask(){
        $this->cms->setActionTitle("所有新闻");
        $page_size=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $news_begin=($page-1)*$page_size;
        $admin_user_id=$this->admin->getTypeId();
//        echo $admin_user_id;
        if($this->admin->check_auth($admin_user_id)){
            $result['news']=$this->article->adminNewsList($page_size,$news_begin);
            $total=$this->article->getRowsTotal();
        }else{
            $result['news']=$this->article->subNewsList($page_size,$news_begin,$admin_user_id);
            $total=$this->article->getSubRowsTotal($admin_user_id);
//            echo $total;
//            var_dump($result);
        }
        $url=WebRequestAnalysis::init()->getQuestion("page=");
        $result['total']=$total;
        $result['page']=new Page($page,$total,$page_size);
        $result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
        $result['type_id']=$this->admin->getTypeId();
        $this->cms->tableScene($result,"admin/tpl/news/show.php");
    }

    function modifyTask(){
        $this->cms->setActionTitle("新闻修改");
        $id=(int)$_GET['id'];
        $result=$this->article->modifyShow($id);
        $this->cms->normalScene($result,"admin/tpl/news/modify.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function modifySubmitTask(){
        $id=(int)$_POST['id'];
        $title=$_POST['title'];
        $author=$_POST['author'];
        $date=$_POST['date'];
        $typeid=$this->admin->getTypeId();
        $content=$_POST['content'];
        $re=$this->article->modifySubmit($id,$title,$author,$date,$typeid,$content);
        if($re){
            $result[0]="修改成功";
        }else{
            $result[0]="修改失败";
        }
        $this->cms->tableScene($result,"admin/tpl/news/modifySubmit.php");
    }
    function deleteTask(){
//        echo "<meta charset='utf-8'/><script>alert('确定删除吗？');window.history.forward();</script>";
        $id=(int)$_GET['id'];
        $re=$this->article->delete($id);
        if($re){
            $result[0]="删除成功";
        }else{
            $result[0]="删除失败";
        }
        $this->cms->tableScene($result,"admin/tpl/news/delete.php");
    }

} 