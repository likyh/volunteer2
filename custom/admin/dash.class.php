<?php
import("Custom.Admin.adminBase");
class dash extends adminBase {
    function indexTask(){
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
        $this->cms->tableScene(array(),"admin/tpl/index.php");
    }
}