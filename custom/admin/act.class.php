<?php
import("custom.data.activityMode");
import("Custom.Admin.adminBase");
import("lib.file.Uploader");
import("lib.common.Page");
class act extends adminBase{
    /** @var activityMode */
    protected $article;
    protected $up;
    protected function onStart(){
        parent::onStart();
        $this->article=activityMode::init();
        $this->up=Uploader::init();
        $this->cms->setControlFile("admin/tpl/act/control.json");
        $this->cms->setPageTitle("活动招募管理");
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
    }
    function showTask(){
        $this->cms->setActionTitle("所有活动招募");
        $page_size=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $act_begin=($page-1)*$page_size;
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $total=$this->article->getRowsTotal();
            $result['act']=$this->article->adminList($page_size,$act_begin);
        }
        else {
            $total=$this->article->getTotal($this->admin->getTypeId());
            $result['act']=$this->article->adminPartList($page_size,$act_begin,$admin_user_id);
        }
        foreach($result['act'] as &$v){
            $v['organizer']=mb_substr($v['organizer'], 0, 4, 'utf-8');
        }
        $url=WebRequestAnalysis::init()->getQuestion("page=");
        $result['page']=new Page($page,$total,$page_size);
        $result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
        $result['level']=$this->admin->getLevel($admin_user_id);
//        var_dump($result);
        $this->cms->tableScene($result,"admin/tpl/act/show.php");

    }
    function createTask(){
        $this->cms->setActionTitle("添加活动");
        $result['author']=$_SESSION['admin']['user']['name'];
        $result['state']=$this->article->state();
        $this->cms->normalScene($result,"admin/tpl/act/create.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function createSubmitTask(){
        if(isset($_POST['name'])&&!empty($_POST['name'])&&isset($_POST['author'])&&!empty($_POST['author'])&&isset($_POST['time'])&&!empty($_POST['time'])&&isset($_POST['place'])&&!empty($_POST['place'])&&isset($_POST['content'])&&!empty($_POST['content'])){
            $name=$_POST['name'];
            $state_id=$_POST['state_id'];
            $nature=$_POST['nature'];
            $typeid=$_SESSION['admin']['user']['typeid'];
            $place=$_POST['place'];
            $organizer=$_POST['organizer'];
            $time=$_POST['time'];
            $description=$_POST['description'];
            $content=$_POST['content'];;
            //图片上传
            list($ret,$upInfo)=$this->up->upFile("picurl");
            if(strtolower($ret)!="success"){
                $result[0]="图片上传失败";
                $picurl=null;
            }else{
                $picurl=$upInfo['url'];
            }
            $re=$this->article->activityCreateSubmit($name,$typeid,$state_id,$nature,$organizer,$time,
                $place,$description,$content,$picurl);
            if($re){
                $result[0]="添加成功";
            }else{
                $result[0]="添加失败（注意活动不能重名，日期格式为2014-01-01）";
            }
            $this->cms->tableScene($result,"admin/tpl/act/createSubmit.php");
        }else{
            echo "<meta charset='utf-8'/><script>alert('信息填写不完整！');window.history.back();</script>";
        }

    }
    public function modifyTask(){
        $this->cms->setActionTitle("活动招募修改");
        $id=(int)$_GET['id'];
        $result=$this->article->activityModifyShow($id);
        $result['id']=$id;
        $result['author']=$_SESSION['admin']['user']['name'];
        $result['state']=$this->article->state();
        $this->cms->normalScene($result,"admin/tpl/act/modify.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    public function modifySubmitTask(){
            $id=(int)$_POST['id'];
            $name=$_POST['name'];
            $state_id=$_POST['state_id'];
            $nature=$_POST['nature'];
            $place=$_POST['place'];
            $organizer=$_POST['organizer'];
            $time=$_POST['time'];
            $description=$_POST['description'];
            $content=$_POST['content'];
            list($ret,$upInfo)=$this->up->upFile("picurl");
            if(strtolower($ret)!="success"){
                $result[0]="图片上传失败";
                $picurl=null;
            }else{
                $picurl=$upInfo['url'];
            }
            $re=$this->article->activityModifySubmit($id,$name,$state_id,$nature,$organizer,$time,$place,$description,$content,$picurl);
            if($re){
                $result[0]="修改成功";
            }else{
                $result[0]="修改失败";
            }
            $this->cms->tableScene($result,"admin/tpl/act/modifySubmit.php");
    }
    public function deleteTask(){
        $id=(int)$_GET['id'];
        if($this->article->getExist($id)){
            echo "<meta charset='utf-8'/><script>alert('此活动有具体信息不能被删除！');window.history.back();</script>";
        }else{
            $re=$this->article->activityDelete($id);
            if($re){
                $result[0]="删除成功";
            }else{
                $result[0]="删除失败";
            }
            $this->cms->tableScene($result,"admin/tpl/act/delete.php");
        }
    }
    public function selectTimeTask(){
        $this->article->setTimeActId($_GET['id']);
        $web=WebRequestAnalysis::init();
        header("Location:".$web->getPage("act","timeShow"));
        exit();
    }
    public function timeShowTask(){
        $id=$this->article->getTimeActId();
        $page_size=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $time_begin=($page-1)*$page_size;
        $total=$this->article->getTimeTotal($id);
        $url=WebRequestAnalysis::init()->getQuestion("page=");
        $result['page']=new Page($page,$total,$page_size);
        $result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
        $result['time']=$this->article->timeShow($id,$page_size,$time_begin);
        $result['total']=$total;
        $result['act_id']=$id;
        $this->cms->tableScene($result,"admin/tpl/act/timeShow.php");
    }
    public function timeDeleteTask(){
        $id=(int)$_GET['id'];
        $re=$this->article->timeDelete($id);
        if($re){
            $result[0]="删除成功";
        }else{
            $result[0]="删除失败";
        }
        $this->cms->tableScene($result,"admin/tpl/act/timeDelete.php");
    }
    public function timeAddTask(){
        $act_id=(int)$_GET['act_id'];//活动id
        $result['act_id']=$act_id;
        $this->cms->formScene($result,"admin/tpl/act/timeAdd.php");
    }
    public function timeAddSubmitTask(){
        if(isset($_POST['date'])&&!empty($_POST['date'])&&isset($_POST['startTime'])&&!empty($_POST['startTime'])&&isset($_POST['endTime'])&&!empty($_POST['endTime'])&&isset($_POST['length'])&&!empty($_POST['length'])&&isset($_POST['total_num'])&&!empty($_POST['total_num'])){
            $date=date("Y-m-d",strtotime($_POST['date']));
            if($date=="1970-01-01"){
                echo "<meta charset='utf-8'/><script>alert('时间格式填写有误！');window.history.back();</script>";
            }else{
                $act_id=$_POST['act_id'];
                $startTime=$_POST['startTime'];
                $endTime=$_POST['endTime'];
                $length=$_POST['length'];
                $total_num=$_POST['total_num'];
                $re=$this->article->timeCreateSubmit($act_id,$date,$startTime,$endTime,$length,$total_num);
                if($re){
                    $result[0]="添加成功";
                }else{
                    $result[0]="添加失败";
                }
                $result[1]=$act_id;
                $this->cms->tableScene($result,"admin/tpl/act/timeAddSubmit.php");
            }
        }else{
            echo "<meta charset='utf-8'/><script>alert('信息填写不完整！');window.history.back();</script>";
        }
    }
    public function selectActTask(){
        $this->article->setUserActId($_GET['id']);
        $web=WebRequestAnalysis::init();
        header("Location:".$web->getPage("act","userShow"));
        exit();
    }
    public function userShowTask(){
        $id=$this->article->getUserActId();//活动时间id
        $page_size=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $news_begin=($page-1)*$page_size;
        $total=$this->article->getUserTotal($id);
        $url=WebRequestAnalysis::init()->getQuestion("page=");
        $result['page']=new Page($page,$total,$page_size);
        $result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
        $result['user']=$this->article->userShow($id,$page_size,$news_begin);
        $result['time_id']=$id;
        $admin_user_id=$this->admin->getTypeId();
        $result['level']=$this->admin->getLevel($admin_user_id);
        $result['total']=$total;
        $this->cms->tableScene($result,"admin/tpl/act/userShow.php");
    }
    public function userAddTask(){
        $time_id=$_GET['time_id'];
        $result['time_id']=$time_id;
        $this->cms->formScene($result,"admin/tpl/act/userAdd.php");
    }
    public function userAddSubmitTask(){
        if(isset($_POST['time_id'])&&!empty($_POST['time_id'])&&isset($_POST['name'])&&!empty($_POST['name'])&&isset($_POST['num'])&&!empty($_POST['num'])){
            $time_id=$_POST['time_id'];
            $name=$_POST['name'];
            $num=$_POST['num'];
            $result=$this->article->userAdd($time_id,$num,$name);
            $result['time_id']=$time_id;
            $this->cms->tableScene($result,"admin/tpl/act/userAddSubmit.php");
        }else{
            echo "<meta charset='utf-8'/><script>alert('信息填写不完整！');window.history.back();</script>";
        }
    }
    public function userAuthTask(){
        $id=$_GET['id'];//activity_user_id
        $time_id=$_GET['time_id'];
        $re=$this->article->userAuth($id);
        if($re){
            $result[0]="审核成功";
        }else{
            $result[0]="审核失败";
        }
        $result[1]=$time_id;
        $this->cms->tableScene($result,"admin/tpl/act/userAuthSubmit.php");
    }
    public function rejectTask(){
        $id=$_GET['id'];//activity_user_id
        $time_id=$_GET['time_id'];
        $re=$this->article->reject($id);
        if($re){
            $result[0]="否决成功";
        }else{
            $result[0]="否决失败";
        }
        $result[1]=$time_id;
        $this->cms->tableScene($result,"admin/tpl/act/rejectSubmit.php");
    }
    public function authTask(){
        $id=$_GET['id'];//活动id
        $re=$this->article->activityAuth($id);
        if($re){
            $result[0]="审核成功";
        }else{
            $result[0]="审核失败";
        }
        $this->cms->tableScene($result,"admin/tpl/act/authSubmit.php");
    }
    public function userDeleteTask(){
        $id=$_GET['id'];//activity_user_id
        $time_id=$_GET['time_id'];
        $re=$this->article->userDelete($id,$time_id);
        if($re){
            $result[0]="删除成功";
        }else{
            $result[0]="删除失败";
        }
        $result[1]=$time_id;
        $this->cms->tableScene($result,"admin/tpl/act/userDelete.php");
    }
}