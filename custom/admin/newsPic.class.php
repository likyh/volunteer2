<?php
import("custom.data.newsPicMode");
import("Custom.Admin.adminBase");
import("lib.file.Uploader");
import("lib.common.Page");
class newsPic extends adminBase{
    /** @var newsPicMode */
    protected $article;
    protected $up;
    protected function onStart(){
        parent::onStart();
        $this->article=newsPicMode::init();
        $this->up=Uploader::init();
        $this->cms->setControlFile("admin/tpl/newsPic/control.json");
        $this->cms->setPageTitle("图片新闻管理");
        $name=$this->admin->getName();
        $this->cms->setUserName($name);
        $admin_user_id=$this->admin->getTypeId();
        if($this->admin->check_auth($admin_user_id)){
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/nav.json"));
        }else{
            $this->cms->loadConfig(array("navFile"=>"admin/tpl/subnav.json"));
        }
    }
    function createTask(){
        $this->cms->setActionTitle("添加图片新闻");
        $result['author']=$this->admin->getCName();
        $this->cms->normalScene($result,"admin/tpl/newsPic/create.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function createSubmitTask(){
        $title=$_POST['title'];
        $author=$_POST['author'];
        $date=$_POST['date'];
        $typeid=$this->admin->getTypeId();
        $content=$_POST['content'];
        list($ret,$upInfo)=$this->up->upFile("picurl");
        if(strtolower($ret)!="success"){
            $result[0]="图片上传失败";
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $re=$this->article->createSubmit($title,$author,$date,$content,$picurl,$typeid);
        if($re){
            $result[0]="添加成功";
        }else{
            $result[0]="添加失败（注意新闻不能重名，日期格式为2014-01-01）";
        }
        $this->cms->tableScene($result,"admin/tpl/newsPic/createSubmit.php");
    }
    function showTask(){
        $this->cms->setActionTitle("所有图片新闻");
        $page_size=getConfig('page','size','site');
        $page=isset($_GET['page'])?(int)$_GET['page']:1;
        $newsPic_begin=($page-1)*$page_size;
        $total=$this->article->getRowsTotal();
        $result['newsPic']=$this->article->getList($page_size,$newsPic_begin);
        $url=WebRequestAnalysis::init()->getQuestion("page=");
        $result['page']=new Page($page,$total);
        $result['page']->setPageCallback(create_function('$page','return "'.$url.'".$page;'));
        $result['type_id']=$this->admin->getTypeId();
        $this->cms->tableScene($result,"admin/tpl/newsPic/show.php");
    }
    function modifyTask(){
        $this->cms->setActionTitle("图片新闻修改");
        $id=(int)$_GET['id'];
        $result=$this->article->modifyShow($id);
        $this->cms->normalScene($result,"admin/tpl/newsPic/modify.php",
            CmsView::TYPE_FORM| CmsView::TYPE_JQUERY| CmsView::TYPE_EDITOR);
    }
    function modifySubmitTask(){
        $id=(int)$_POST['id'];
        $title=$_POST['title'];
        $author=$_POST['author'];
        $date=$_POST['date'];
        $typeid=$_POST['typeid'];
        $content=$_POST['content'];
        list($ret,$upInfo)=$this->up->upFile("picurl");
        if(strtolower($ret)!="success"){
            $result[0]="图片上传失败";
            $picurl=null;
        }else{
            $picurl=$upInfo['url'];
        }
        $re=$this->article->modifySubmit($id,$title,$author,$date,$content,$picurl,$typeid);
        if($re){
            $result[0]="修改成功";
        }else{
            $result[0]="修改失败";
        }
        $this->cms->tableScene($result,"admin/tpl/newsPic/modifySubmit.php");
    }
    function deleteTask(){
        $id=(int)$_GET['id'];
        $re=$this->article->delete($id);
        if($re){
            $result[0]="删除成功";
        }else{
            $result[0]="删除失败";
        }
        $this->cms->tableScene($result,"admin/tpl/newsPic/delete.php");
    }
} 