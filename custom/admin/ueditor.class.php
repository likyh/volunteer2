<?php
import("Custom.Admin.adminBase");
import("lib.file.FileObject");
import("lib.file.Uploader");
class ueditor extends adminBase {
    /** @var  Uploader */
    protected $up;
    protected function onStart(){
        parent::onStart();
        $this->up=Uploader::init();
    }

    public function indexTask(){
        $action = $_GET['action'];
        switch ($action) {
            case 'config':
                $this->devolve("configTask");
                break;

                /* 上传图片 */
            case 'uploadimage':
                /* 上传视频 */
            case 'uploadvideo':
                /* 上传文件 */
            case 'uploadfile':
                $this->devolve("fileTask");
                break;

            /* 列出图片 */
            case 'listimage':
                $this->devolve("imageListTask");
                break;
            /* 列出文件 */
            case 'listfile':
                $this->devolve("fileListTask");
                break;

            /* 抓取远程文件 */
            case 'catchimage':
                //TODO　远程抓取图片功能未完成
//                $this->devolve("catchTask");
//                break;

            default:
                View::displayAsJson(array(
                    'state'=> '请求地址出错'
                ));
                break;
        }
    }

    public function configTask(){
        echo preg_replace("/\/\*[\s\S]+?\*\//", "", FileObject::get("config/ueditor.json"));
    }

    public function fileTask(){
        list($state,$info)=$this->up->upFile("upfile");
        $info['state']=$state;
        View::displayAsJson($info);
    }

    public function catchTask(){
        list($state,$info)=$this->up->upFile("upfile");
        $info['state']=$state;
        View::displayAsJson($info);
    }

    function imageListTask(){
        $this->fileListTask("imageAllow");
    }

    function fileListTask($allowFiles=null){
        function getfiles($path, &$allowFiles, &$files = array()){
            if (!is_dir($path)) return null;
            if(substr($path, strlen($path) - 1) != '/') $path .= '/';
            $handle = opendir($path);
            while (false !== ($file = readdir($handle))) {
                if ($file != '.' && $file != '..') {
                    $path2 = $path . $file;
                    if (is_dir($path2)) {
                        getfiles($path2, $allowFiles, $files);
                    } else {
                        if (in_array(FileObject::fileExt($file),$allowFiles)) {
                            $files[] = array(
                                'url'=> getConfig("site","root").$path2,
                                'mtime'=> filemtime($path2)
                            );
                        }
                    }
                }
            }
            return $files;
        }

        $path=getConfig("uploader","savePath");
        if(empty($allowFiles)) $allowFiles="documentAllow";
        $allowFiles=explode(",",getConfig("uploader",$allowFiles));
        $list=getfiles($path,$allowFiles);
        $result = array(
            "state" => "SUCCESS",
            "list" => $list,
            "start" => 0,
            "total" => count($list)
        );
        View::displayAsJson($result);
    }
} 