<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>中国矿业大学志愿服务网</title>
    <base href="<?php echo $system['siteRoot'];?>" />
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/commom.css"/>
    <link rel="stylesheet" type="text/css" href="style/activity.css"/>

    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/activity.js"></script>
</head>

<body>
<?php import_part("Custom.module","header"); ?>

<div class="mainPart">
	<div id="content">
		<div class="locateNav">
		当前位置:
		<a href="<?php e_page("home", "index"); ?>">网站首页</a>
		>
		<a href="<?php e_page("act", "list"); ?>">公益活动报名</a>
		> 活动内容
		</div>
		<div id="article">
			<div id="articleTitle">
				<h2><?php echo $r['content']['name']  ?></h2>
				<?php import_part("Custom.module", "share"); ?>
            </div>
            <div id="articleContent">
                <div id="activityImg"><img src="<?php echo $r['content']['picurl'] ?>"></div>
                <div id="activityIntro">
                    <ul>
                        <li>主办方：<span><?php echo $r['content']['organizer']  ?></span></li>
                        <li>发布组织：<span><?php echo $r['content']['author']  ?></span></li>
                        <li>活动状态：<span class="state"><?php echo $r['content']['state']  ?></span></li>
                        <li>活动地点<span>：<?php echo $r['content']['place']  ?></span></li>
                        <li>活动时间：<span><?php echo $r['content']['time']  ?></span></li>
                    </ul>
                    <div class="mybody">
                        <div class="botton" id="joinButton"><button type="button">报名参加</button></div>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="article_content">
                <h3>活动简介</h3>
                    <p><?php echo $r['content']['content'] ?></p>
                </div>

                <h3>活动报名时间</h3>
                <div id="actTimeApply">
                <?php if(empty($r['time'])){ ?><ul>暂时不能报名</ul><?php } else {?>
                    <ul><?php foreach($r['time'] as $key=>$value) {?>
                        <li> → <?php echo $value['start'];?> - <?php echo $value['end'] ?> <a name="actTimeApply"  href="<?php e_page( "act", "actApply",array('activity_id'=>$r['content']['id'],'time_id'=>$value['id'])); ?>">报名</a></li>
                        <?php } ?>
                    </ul><?php }?>
                </div>

                <h3>附件下载</h3>
                <div id="down_load"><?php if(!empty($r['content']['attachment'])){?>点击<a href="<?php echo $r['content']['attachment'] ; ?>">这里</a>下载附件<?php } else {echo "没有附件";}?></div>
            </div>
            <div id="pageChange">
                <?php if($r['preId']!=null) {?><div id="previous">上一篇：<a href="<?php e_action("content",array('id' =>$r['preId'] )); ?>"><?php echo $r['preName'] ?></a></div><?php }?>
                <?php if($r['nextId']!=null) {?><div id="next">下一篇：<a href="<?php e_action("content",array('id' =>$r['nextId'] )); ?>"><?php echo $r['nextName']  ?></a></div><?php }?>
                <div class="clear"></div>
            </div>
		</div>
	</div>

    <?php import_part("Custom.module","rail"); ?>
    <div class="clear"></div>
</div>

<footer>
	<?php import_part("Custom.module","tail"); ?>
</footer>
</body>
</html>
