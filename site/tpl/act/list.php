<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>中国矿业大学志愿服务网</title>
    <base href="<?php echo $system['siteRoot'];?>" />

    <link rel="stylesheet" type="text/css" href="style/commom.css"/>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/list.css"/>
</head>

<body>
<?php import_part("Custom.module","header"); ?>

<div class="mainPart">
	<div id="content">
		<div class="locateNav">
		当前位置:
		<a href="<?php e_page("home", "index"); ?>">网站首页</a>
		&gt;
		<a href="<?php e_page("act", "list"); ?>">公益活动报名</a>
		</div>
		<div id="list">
			<h2>公益活动报名</h2>	
			<div class="textSelect">
				<form name="form" action="HandleFormInput.jsp">
				<dl>
					<dt>发布组织：</dt>
					<dd><a class="<?php if(!isset($r['s']['author'])||$r['s']['author']=='all'||empty($r['s']['author'])) {echo "selected";}?>" href="<?php e_action("list",array('author'=>'all')+$r['s'])?>">全部</a></dd>
					<dd><a class="<?php if(isset($r['s']['author'])&&$r['s']['author']=='school'){echo "selected";}?>" href="<?php e_action("list",array('author'=>'school')+$r['s'])?>">校青协</a></dd>
					<dd><a class="<?php if(isset($r['s']['author'])&&$r['s']['author']=='academy'){echo "selected";}?>" href="<?php e_action("list",array('author'=>'academy')+$r['s'])?>">院青协</a></dd>
					<dd><a class="<?php if(isset($r['s']['author'])&&$r['s']['author']=='others'){echo "selected";}?>" href="<?php e_action("list",array('author'=>'others')+$r['s'])?>">其它志愿组织</a></dd>
				</dl><br/>
				<dl>
					<dt>活动状态：</dt>
					<dd><a class="<?php if(!isset($r['s']['state'])||$r['s']['state']=='all'||$r['s']['state']=='') {echo "selected";}?>" href="<?php e_action("list",array('state'=>'all')+$r['s'])?>">全部</a></dd>
					<dd><a class="<?php if(isset($r['s']['state'])&&$r['s']['state']==1){echo "selected";}?>" href="<?php e_action("list",array('state'=>1)+$r['s'])?>">招募中</a></dd>
					<dd><a class="<?php if(isset($r['s']['state'])&&$r['s']['state']==2){echo "selected";}?>" href="<?php e_action("list",array('state'=>2)+$r['s'])?>">招募结束</a></dd>
                    <dd><a  class="<?php if(isset($r['s']['state'])&&$r['s']['state']==3){echo "selected";}?>" href="<?php e_action("list",array('state'=>3)+$r['s'])?>">招募暂停</a></dd>
				</dl><br/>
				</dl>
				<dl>
					<dt>活动时间：</dt>
                    <dd><a class="<?php if(!isset($r['s']['date'])||$r['s']['date']=='all'||$r['s']['date']=='') {echo "selected";}?>" href="<?php e_action("list",array('date'=>"all")+$r['s'])?>">全部</a></dd>
                    <dd><a class="<?php if(isset($r['s']['date'])&&$r['s']['date']=="week"){echo "selected";}?>" href="<?php e_action("list",array('date'=>"week")+$r['s'])?>">最近一周</a></dd>
                    <dd><a class="<?php if(isset($r['s']['date'])&&$r['s']['date']=="month"){echo "selected";}?>" href="<?php e_action("list",array('date'=>"month")+$r['s'])?>">最近一月</a></dd>
                    <dd><a class="<?php if(isset($r['s']['date'])&&$r['s']['date']=="3month"){echo "selected";}?>" href="<?php e_action("list",array('date'=>"3month")+$r['s'])?>">最近三月</a></dd>
                    <dd><a class="<?php if(isset($r['s']['date'])&&$r['s']['date']=="year"){echo "selected";}?>" href="<?php e_action("list",array('date'=>"year")+$r['s'])?>">最近一年</a></dd>
				</dl>
				</form>
			</div>
		</div>	
		<div class="clear"></div>
		<div id="activityList">
		<table>
			<tr>
				<th>活动名称</th>
				<th style="min-width: 100px;">发布组织</th>
				<th style="min-width: 100px;">活动状态</th>
				<th style="min-width: 100px;" class="actTime">活动时间</th>
				<th style="min-width: 50px;" class="see">查看</th>
			</tr>

			<?php if(isset($r['list'])&&!empty($r['list'])){ foreach($r['list'] as $key=>$value) {   ?>
			<tr <?php if((float)$key%2!=0) {echo 'class="'."odd".'"';}?>>
				<td class="actNum"><a href="<?php e_action("content",array('id'=>$value['id'])); ?>"><?php echo $value['name'] ?></a></td>
				<td class="actTeam"><?php echo $value['author'] ?></td>
				<td class="actState"><?php echo $value['state']  ?></td>
				<td class="actTime"><?php echo $value['time']  ?></td>
				<td class="see"><a href="<?php e_action("content",array('id'=>$value['id'])); ?>">详情</a></td>
			</tr><?php }}?>

		</table>
		</div>
		<div class="clear"></div>
		<div id="pageTurn">
            <ul>
                <?php foreach( $r['pageArray'] as $value ){ ?>
                    <li class="<?php if($r['currentPage']==$value) echo 'active' ?>"><a href="<?php e_action("list","page=$value");?>"><?php echo $value?></a></li>
                <?php  } ?>
            </ul>
        </div>
	</div>

    <?php import_part("Custom.module","rail"); ?>
    <div class="clear"></div>
</div>

<footer>
	<?php import_part("Custom.module","tail"); ?>
</footer>
</body>
</html>
