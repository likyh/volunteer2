<div id="publicGroup" class="module">
    <div class="title">
        <h3><a href="<?php e_page("group", "list"); ?>">公益团体&nbsp;<span>Group</span></a></h3>
        <p class="more"><a href="<?php e_page("group", "list"); ?>">更多More</a></p>
    </div>

    <ul>
        <?php foreach ($r as $key => $value) { ?>
            <li class="title">
                <a href="<?php e_page("group", "content", array('id' => $value['id'])); ?>"><?php echo $value['title'] ?></a>
            </li>
        <?php } ?>
    </ul>
</div>