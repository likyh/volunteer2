<div id="projectDisplay" class="module">
	<div class="title">
		<h3><a href="<?php e_page("project", "list"); ?>">志愿项目展示<span>Project Presentation</span></a></h3>
		<p class="more"><a href="<?php e_page("project", "list"); ?>">更多More</a></p>
    </div>
	<ul>
	<?php foreach($r as $key=>$value) { ?>
	<li class="project">
        <img src="<?php echo $value['picurl']?>" class="projectImg"/>
		<div class="project_text">
			<span class="projectName">
                <a href="<?php e_page("project", "content",array('id'=>$value['id'])); ?>">
                <?php echo $value['name']?>
                </a>
            </span><br>
			<span class="projectTimePlace">服务时间：<?php echo $value['time']?></span><br>
			<span class="projectTimePlace">服务地点：<?php echo $value['place']?></span>		
		</div>
	</li><?php } ?>

	</ul>
</div>