<?php load_language("common"); ?>
<header>
<div id="header">
    <div class="topBox">
        <div class="top">
            <?php if(empty($r['username'])){ ?>
                <div class="user">
                    <a href="<?php e_page("user", "login");?>">这里登陆</a>，新用户
                    <a href="<?php e_page("user", "register");?>">注册</a></div>
            <?php }else{?>
                <div class="user logined">
                    欢迎你，<a href="<?php e_page("user", "index");?>"><?php  echo $r['username'];?></a>！
                    <a href="<?php e_page("user", "logout");?>">退出</a>
                </div>
            <?php }?>

            <div class="newsNotice"><?php echo $r['notice']['content'];?></div>
            <div id="searchNews">
                <form action="<?php e_page("news", "list");?>" name="search">
                    <table border="0" cellpadding="0" cellspacing="0" class="tab_search">
                        <tr>
                            <td>
                                <input type="text" name="q" title="Search" class="searchinput" id="searchinput" onkeydown="if (event.keyCode==13) {}" onblur="if(this.value=='')value='-输入关键词搜索-';" onfocus="if(this.value=='-输入关键词搜索-')value='';" value="-输入关键词搜索-" size="10"/>
                            </td>
                            <td>
                                <input type="image" width="21" height="17" class="searchaction" onclick="if(document.forms['search'].searchinput.value=='- Search Products -')document.forms['search'].searchinput.value='';" alt="Search" src="http://www.codefans.net/jscss/demoimg/201008/magglass.gif" border="0" hspace="2"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="share">关注我们：
                <a class="renren" href="<?php echo $l['common']['contact']['renren'] ?>" title="中国矿业大学青年志愿者协会人人主页"><img src="images/renren.png"></a>
                <a class="weibo" href="<?php echo $l['common']['contact']['weibo'] ?>" title="中国矿大青年志愿者协会微博主页"><img src="images/weibo2.png"></a>
            </div>
        </div>
    </div>
	
	<div class="headTitle">
        <h1><span><?php echo $l['common']['title']['title'] ?></span></h1>
    	<nav>
    		<div class="menu">
               <ul>
                <li><a href="<?php e_page("home", "index"); ?>"><?php echo $l['common']['nav']['index'] ?></a></li>
                <li><a href="<?php e_page("news", "list"); ?>"><?php echo $l['common']['nav']['news'] ?></a></li>
                <li><a href="<?php e_page("act", "list"); ?>"><?php echo $l['common']['nav']['activity'] ?></a></li>
                <li><a href="<?php e_page("group", "list"); ?>"><?php echo $l['common']['nav']['group'] ?></a></li>
                <li><a href="<?php e_page("plan", "list"); ?>"><?php echo $l['common']['nav']['plan'] ?></a></li>
                <li><a href="<?php e_page("download", "list"); ?>"><?php echo $l['common']['nav']['download'] ?></a></li>
    		  </ul>
            </div>
    	</nav>	
    </div>

</div>
</header>