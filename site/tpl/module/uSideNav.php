<div class="menu_user">
<ul class="nav">
	<li><a href="<?php e_page("home", "index"); ?>">返回首页</a></li>
	<li><a href="<?php e_page("user", "index") ?>">个人主页</a></li>
	<li><a href="<?php e_action("alterInfo") ?>">个人资料修改</a></li>
	<li><a href="<?php e_page("user", "alterPass") ?>">密码修改</a></li>
	<li><a href="<?php e_action("myList") ?>">已报名的活动</a></li>
	<li><a href="<?php e_action("rank") ?>">服务时长排名</a></li>
	<li><a href="<?php e_page("user", "introduction") ?>">使用帮助</a></li>
</ul>
</div>