	<div id="VolunteersRecruit" class="module">
		<div class="title">
			<h3><a href="<?php e_page("act", "list"); ?>">公益活动报名&nbsp;<span>Volunteers Recruit</span></a></h3>
			<p class="more"><a href="<?php e_page("act", "list"); ?>">更多More</a></p>
		</div>
		<div id="projectSearch">
				<p>活动搜索</p>
				<form action="<?php e_page("act", "list");?>" method="get">    
				<select class="team" name="author">  
					  <option value ="0">[发布组织]</option>  
					  <option value ="school">校青协</option>
					  <option value ="academy">院青协</option>  
					  <option value ="others">其他志愿组织</option>
				</select>  
				<select class="state" name="state">  
				      <option value ="0">[活动状态]</option>  
					  <option value ="1">招募中</option>
					  <option value ="2">招募结束</option>  
					  <option value ="3">招募暂停</option>   
				</select>  
					<select class="state" name="date">  
				      <option value ="0">[活动时间]</option>  			  
					  <option value ="week">最近一周</option>
					  <option value ="month">最近一月</option>  
					  <option value ="3month">最近三月</option>  
					  <option value ="year">最近一年</option>  
				</select>  
				<input type="text" name="s" class="sinput" placeholder="输入关键字">      <input type="submit" value="搜索" class="search_sbtn">
				</form>
		</div>		
		
		<div class="clear"></div>
		<ul id="recruit"><?php foreach($r['activity'] as $key=>$value ) {?>
		<li class="recruit">
			<span class="activityName"><a href="<?php e_page("act", "content",array('id'=>$value['id'])); ?>"><?php echo $value['name'] ?></a></span>
			<span class="state"><?php echo $value['state'] ?></span>
			<span class="organizerTime">发布组织：<?php echo $value['author'] ?></span>
			<span class="organizerTime">活动时间：<?php echo $value['time'] ?></span>
		</li><?php } ?>
		</ul>	
	</div>