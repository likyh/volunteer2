<div id="volunteer" class="module">
    <div class="title">
	   <h3><a>志愿者风采<span>Mien of Volunteer</span></a></h3>
    </div>

    <div id="volunteerPhotoBox">
        <ul id="volunteerPhoto">
            <?php foreach($r as $key=>$value){?>
                <li class="volunteerPhoto"><img src="<?php echo $value['picurl'] ?>"/><span><?php echo $value['title'] ?></span></li>
            <?php } ?>
        </ul>
    </div>

    <ul class="pages"></ul>
</div>