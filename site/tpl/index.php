<!DOCTYPE html>
<html>
<head>
    <meta charset="utf8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>首页 中国矿业大学志愿服务网</title>
    <base href="<?php echo $system['siteRoot'];?>" />
    <meta name="viewport" content="width=480,initial-scale=1" />
    <meta name="viewport" content="width=480,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta  name="keywords" content="中国矿业大学 志愿者 服务网">
    <meta name="description" content="这是中国矿业大学校青协和Likyh团队组织的志愿者服务网，欢迎大家的浏览。" >
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/picnewsSlider.css"/>
    <link rel="stylesheet" type="text/css" href="style/commom.css"/>
    <link rel="stylesheet" type="text/css" href="style/index.css"/>
    <link rel="stylesheet" type="text/css" href="style/banner.css"/>
    <script src="script/jquery-1.10.2.min.js"></script>
    <script src="script/plugins.sly.js"></script>
    <script src="script/sly.min.js"></script>
    <script src="script/banner.js"></script>
    <script src="script/index.js"></script>
</head>

<body>
<div id="container">
<?php import_part("Custom.module","header"); ?>
<div class="mainPart">
    <div id="news">
        <?php import_part("Custom.module","iLogin"); ?>
        <?php import_part("Custom.module","picShow"); ?>
        <?php import_part("Custom.news","newsModule"); ?>
    </div>
    <div id="activity">
        <?php import_part("Custom.module","iActivity"); ?>
        <?php import_part("Custom.module","rank"); ?>
    </div>
    <div id="right_contect">
        <?php import_part("Custom.group","module"); ?>
        <?php import_part("Custom.module","plan"); ?>
        <?php import_part("Custom.module","download"); ?>
    </div>
    <?php import_part("Custom.module","iProject"); ?>
    <?php import_part("Custom.module","mien"); ?>
    <?php import_part("Custom.module","friendLink"); ?>
</div>

<footer>
    <?php import_part("Custom.module","tail"); ?>
</footer>

</body>
</html>