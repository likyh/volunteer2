<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>中国矿业大学志愿服务网</title>
    <base href="<?php echo $system['siteRoot'];?>" />

    <link rel="stylesheet" type="text/css" href="style/commom.css"/>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/activity.css"/>

</head>

<body>
<?php import_part("Custom.module","header"); ?>

<div class="mainPart">
	<div id="content">
		<div class="locateNav">
		当前位置:
		<a href="<?php e_page("home", "index"); ?>">网站首页</a>
		>
		<a href="<?php e_page("project", "list"); ?>">志愿活动项目</a>
		> 活动详情
		</div>
		<div id="article">
			<div id="articleTitle">
				<h2><?php echo $r['name']?></h2>
				<?php import_part("Custom.module","share"); ?>
            </div>
            <div id="articleContent">
                <div id="activityImg"><img src="<?php echo $r['picurl']  ?>"></div>
                <div id="activityIntro">
                    <ul>
                        <li>主办方：<span><?php echo $r['organizer'] ?></span></li>
                        <li>活动地点<span>：<?php echo $r['place'] ?></span></li>
                        <li>活动时间：<span><?php echo $r['time'] ?></span></li>
                    </ul>
                </div>
                <div class="clear"></div>
                <div class="article_content">
                    <h3>活动简介</h3>
                    <p><?php echo $r['content'] ?></p>
                </div>
            </div>
			<div id="pageChange">
				<?php if($r['preId']!=null) {?><div id="previous">上一篇：<a href="<?php e_action("content",array('id' =>$r['preId'] )); ?>"><?php echo $r['preName'] ?></a></div><?php }?>
				<?php if($r['nextId']!=null) {?><div id="next">下一篇：<a href="<?php e_action("content",array('id' =>$r['nextId'] )); ?>"><?php echo $r['nextName']  ?></a></div><?php }?>
			</div>
		</div>
	</div>

    <?php import_part("Custom.module","rail"); ?>
    <div class="clear"></div>
</div>

<footer>
	<?php import_part("Custom.module", "tail"); ?>
</footer>
</body>
</html>
