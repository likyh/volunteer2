<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>中国矿业大学志愿服务网</title>
    <base href="<?php echo $system['siteRoot'];?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

    <link rel="stylesheet" type="text/css" href="style/commom.css"/>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/list.css"/>
</head>

<body>
<?php import_part("Custom.module","header"); ?>

<div class="mainPart">
	<div id="content">
		<div class="locateNav">
            当前位置:
            <a href="<?php e_page("home", "index"); ?>">网站首页</a>
            >
            <a href="<?php e_page("project", "list"); ?>">志愿项目展示</a>
		</div>
		<div id="list">
			<h2>志愿项目展示</h2>
			<ul id="textList">
			<?php foreach($r['list'] as $key=>$value ){ // var_dump($value);?>		
				<li class="project">
					<a href="<?php e_page("project", "content","{$value['id']}"); ?>"><img src="<?php echo $value['picurl'] ?>" class="projectImg"/></a>
					<div class="project_text">
						<span class="projectName"><a href="<?php e_page("project", "content",array('id'=>$value['id'])); ?>"><?php echo $value['name']  ?></a></span><br>
						<span class="projectTimePlace">主办方：<?php echo $value['organizer']  ?></span><br>
						<span class="projectTimePlace">服务时间：<?php echo $value['time']  ?></span><br>
						<span class="projectTimePlace">服务地点：<?php echo $value['place']  ?></span><br>
						<span class="projectTimePlace">简介：<?php echo mb_substr($value['content'],0,30,'utf-8')  ?>...</span>
					</div>
				</li>
			<?php }?>
			</ul>
		</div>
		<div id="pageTurn">
            <ul>
            <?php foreach( $r['pageArray'] as $value ){ ?>
                <li class="<?php if($r['currentPage']==$value) echo 'active' ?>"><a href="<?php e_action("list","page=$value");?>"><?php echo $value?></a></li>
            <?php  } ?>
            </ul>
		</div>
	</div>

    <?php import_part("Custom.module","rail"); ?>
    <div class="clear"></div>
</div>

<footer>
	<?php import_part("Custom.module","tail"); ?>
</footer>
</body>
</html>
