<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>中国矿业大学志愿服务网</title>
    <base href="<?php echo $system['siteRoot'];?>" />

    <link rel="stylesheet" type="text/css" href="style/commom.css"/>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/list.css"/>
</head>

<body>
<?php import_part("Custom.module","header"); ?>

<div class="mainPart">
    <div id="content">
        <div class="locateNav">
            当前位置:
            <a href="<?php e_page("home", "index"); ?>">网站首页</a>
            >
            <a href="<?php e_page("group", "catalog"); ?>"><?php echo $result['pageTitle']?></a>
        </div>
        <div id="list">
            <h2><?php echo $result['pageTitle']?></h2>
            <div class="textSelect">
                <dl>
                    <dt>文章分类：</dt>
                    <dd><a class="<?php if($r['type']=="all") echo 'selected';?>" href="<?php e_action("list","type=all")?>">全部</a></dd>
                    <dd><a class="<?php if($r['type']=="xqx") echo 'selected';?>" href="<?php e_action("list","type=xqx")?>">校青协</a></dd>
                    <dd><a class="<?php if($r['type']=="other") echo 'selected';?>" href="<?php e_action("list","type=other")?>">其它</a></dd>
                </dl>
            </div>
            <ul id="textList">
                <?php foreach( $r['list'] as $key=>$value ){ ?>
                    <li><a href="<?php e_action("content",array('id'=>$value['id'])); ?>">
                            <span class="orginazition"> </span><?php echo $value['title'];?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div id="pageTurn">
            <ul>
                <?php foreach( $r['pageArray'] as $value ){ ?>
                    <li class="<?php if($r['currentPage']==$value) echo 'active' ?>"><a href="<?php e_action("list","page=$value");?>"><?php echo $value?></a></li>
                <?php  } ?>
            </ul>
        </div>
    </div>

    <?php import_part("Custom.module","rail"); ?>
    <div class="clear"></div>
</div>

<footer>
    <?php import_part("Custom.module","tail"); ?>
</footer>

</body>
</html>
