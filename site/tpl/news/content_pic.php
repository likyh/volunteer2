<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>中国矿业大学志愿服务网</title>
    <base href="<?php echo $system['siteRoot'];?>" />

    <link rel="stylesheet" type="text/css" href="style/commom.css"/>
    <link rel="stylesheet" type="text/css" href="style/reset.css"/>
    <link rel="stylesheet" type="text/css" href="style/activity.css"/>

</head>

<body>
<?php import_part("Custom.module","header"); ?>
<div class="mainPart">
    <div id="content">
        <div class="locateNav">
            当前位置:
            <a href="<?php e_action("index"); ?>">网站首页</a>
            >
            图片新闻
            > <?php echo $r['title']?>
        </div>

        <div id="article">
            <div id="articleTitle"  class="article_content">
                <h2><?php echo $r['title']?></h2>

                <?php import_part("Custom.module","share"); ?>
            </div>
            <div id="articleContent">
                <p><?php echo $r['content'] ?></p>
            </div>
            <div id="pageChange">
                <?php if($r['preId']!=null) {?><div id="previous">上一篇：
                    <a href="<?php e_action("content","id={$r['preId']}"); ?>">
                        <?php echo $r['preTitle'] ?></a></div><?php }?>
                <?php if($r['nextId']!=null) {?><div id="next">下一篇：
                    <a href="<?php e_action("content","id={$r['nextId']}"); ?>">
                        <?php echo $r['nextTitle'] ?></a></div><?php }?>
            </div>
        </div>
    </div>

    <?php import_part("Custom.module","rail"); ?>
    <div class="clear"></div>
</div>

<footer>
    <?php import_part("Custom.module","tail"); ?>
</footer>

</body>
</html>