<div id="picnews" class="picnews">
    <div class="slideBox">
        <ul class="items">
        <?php foreach($r['list'] as $value) {?>
            <li><a href="<?php e_page("news", "content_pic","id={$value['id']}"); ?>" title="<?php echo $value['title'];?>">
                <img src="<?php echo $value['picurl'] ?>" alt="<?php echo $value['title'];?>"/>
            </a></li>
        <?php } ?>
        </ul>
    </div>
</div>