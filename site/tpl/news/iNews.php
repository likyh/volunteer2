<div class="module news">
    <div class="title">
        <h3><a href="<?php e_page("news", "list"); ?>">新闻资讯&nbsp;<span>News</span></a></h3>
        <p class="more"><a href="<?php e_page("news", "list"); ?>">更多More</a></p>
    </div>
    <ul>
    <?php foreach ($r as $key => $value) { ?>
        <li class="title">
            <a href="<?php e_page("news", "content", array('id' => $value['id'])); ?>">
                <span class="orginazition">[<?php echo $value['type'] ?>]</span>
                <?php echo $value['title'] ?></a>
            <span class="news_time"><?php echo $value['date'] ?></span>
        </li>
    <?php } ?>
    </ul>
</div>