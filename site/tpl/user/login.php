<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8>
<title>用户登录-矿大志愿服务网</title>
<base href="<?php echo $system['siteRoot'];?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<link rel="stylesheet" type="text/css" href="style/login.css"/>
<script type="text/javascript" src="js/register.js"></script>

</head>

<body>
<header>
<?php import_part("Custom.module","header"); ?>
</header>

<div class="login_Part">

	<div id="area">
		<div class="login_single">		
			<form method='post' action='<?php e_page("user", "loged") ?>'>
			<ul>
			<legend>用户登录</legend>
				<li><label for="username">用户名</label> <input type="text" name="username" placeholder="请输入8位学号" id="username"/></li>
				<li><label for="password">密 &nbsp; 码</label> <input type="password" name="password" placeholder="请输入密码" id="password"/></li>
			</ul>
			<button type="submit" class="sbtn" id="login_sbtn">登 陆</button>
			</form>
			<div class="register">第一次登陆?先进行<a href="<?php e_page("user", "register"); ?>">注册</a>吧 <a href="#">忘记密码？</a></div>
		</div>
	</div>
</div>
<div class="clear"></div>
<footer>
<!-- BEGIN #tail -->
	<?php import_part("Custom.module","tail"); ?>
<!-- END #tail -->

</footer>
　
<!--[if lt IE 9]>
　　　　<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

</body>
</html>
