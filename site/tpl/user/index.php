<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>志愿服务网-个人主页</title>
<base href="<?php echo $system['siteRoot'];?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/personal.css"/>
<link rel="stylesheet" type="text/css" href="style/register.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
<script src="script/jquery.min.js"></script>
</head>

<body>
	<?php import_part("Custom.module","uTopNav"); ?>
		<div id="myHome">		
	<?php import_part("Custom.module","uSideNav"); ?>
			
			<div class="myHome_content">

		<!--BEGIN个人主页-->
				<div class="locateNav">
					<span>当前位置:</span>
					<a href="<?php e_page("user", "index") ?>">个人首页</a>
				</div>
					<div class="contentField">
						<h3>其它招募信息</h3>
						<ul class="textList"><?php foreach($r['activity_catalog'] as $key=>$value) {?>
							<li><a href="<?php e_page("act","content",array('id' =>$value['id']))?>"><?php echo $value['name']; ?></a></li>
						<?php }?>		
						</ul>
						<h3>相关资料下载</h3>
						<div id="related_download">
							<ul class="textList"><?php foreach($r['download_catalog'] as $key=>$value) {?>
							<li><a href="<?php e_page("download", "content",array('id' =>$value['id']))?>"><?php echo $value['title']; ?></a></li>
							<?php }?>
							</ul>
						</div>
				</div>
		<!--END个人主页-->

	</div>
</div>
　<!--[if lt IE 9]>
　　　　<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
　　<![endif]-->
</body>
</html>
