<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8>
<title>中国矿业大学志愿服务网</title>
<base href="<?php echo $system['siteRoot'];?>" />

<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<link rel="stylesheet" type="text/css" href="style/register.css"/>
<script src="surface/js/jquery-1.9.1.js"></script>
<script src="surface/js/jquery-validate.js"></script>
<script src="surface/js/check.register.js"></script>
</head>

<body>
<!--BEGIN #header-->
<?php import_part("Custom.module","header"); ?>
<!--END #header-->

<div class="mainPart">
<div id="area">
	<h2>第一步</h2>
	<div id="item">
		<h3>阅读注册条款</h3>
		<textarea readonly>（一）报名志愿者基本条件
	（1）具有奉献精神；
	（2）具备与所参加的志愿服务项目及活动相适应的基本素质；
	（3）遵守法律、法规以及志愿服务组织的章程和其他管理制度；
	（4）未成年人员参加志愿服务，应当征得其监护人的同意，参加与其年龄、身心状况相适应的志愿服务活动；
	注册志愿者还应具备以下条件：
	（1）愿意长期参加志愿服务；
	（2）原则上每年志愿服务不少于 20 小时。
（二）报名志愿者的权利
	（1）参加志愿服务组织的志愿服务活动；
	（2）接受与志愿服务有关的知识和技能培训；
	（3）对志愿服务组织的工作进行监督，提出建议、批评和意见；
	（4）获得所参加志愿服务活动的相关信息；
	（5）获得参加志愿服务活动所必要的物质、安全保障；
	（6）自身有志愿服务需求时优先获得志愿服务；
	（7）志愿者可以申请退出志愿服务组织；
	（8）法律、法规以及志愿服务组织章程规定的其他权利。
（三） 报名对象的义务
	（1）遵守志愿服务组织的章程和其他管理制度；
	（2）履行志愿服务承诺，参加志愿服务组织安排的志愿服务工作；
	（3）不损害志愿服务对象的合法权益，保守志愿服务对象的隐私和秘密；
	（4）不得向志愿服务对象收取或者变相收取报酬；
	（5）维护志愿服务组织和志愿者的形象，不以志愿者身份从事营利性或者违背社会公德的活动；
	（6）法律、法规以及志愿服务组织章程规定的其他义务。</textarea>
	</div>
	<div id="activate">
	<h3>激活身份信息<span>（自动匹配教务系统信息注册！）</span></h3>
	<ul>
		<form  method="post" name="send" action="<?php e_action("regInfo"); ?>">
			<li><label for="username">姓名</label>
				<input type="text" name="username" id="username" data-validate="required" data-describedby="nameMsg" data-description="username" /><div class="massages" >
			<div id="nameMsg"></div></div></li>
			<li><label for="userNum1">学号</label>
				<input type="text" name="num" id="userNum1" data-validate="num" data-describedby="numMsg" data-description="userNum1" /><div class="massages" ><div id="numMsg"></div>
		</div></li>
			<input type="submit" value="提交" class="btn"/>
		</form>
	</ul>
		
	</div>
	<div class="clear"></div>
</div>

</div>

<footer>
<!--BEGIN #tail-->
	<?php import_part("Custom.module","tail"); ?>
<!--END #tail-->

</footer>
<!--[if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
<!-- <script src="surface/js/jquery.simpleValidate.js"></script> -->

</body>
</html>