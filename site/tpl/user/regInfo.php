<!DOCTYPE html>
<html>
<head>
<meta charset=utf-8>
<title>中国矿业大学志愿服务网</title>
<base href="<?php echo $system['siteRoot'];?>" />

<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<link rel="stylesheet" type="text/css" href="style/register.css"/>
<script src="surface/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="surface/js/jquery-validate.js" type="text/javascript"></script>
<script src="surface/js/check.register.js" type="text/javascript"></script>

</head>

<body>
<!--BEGIN #header-->
<?php import_part("Custom.module","header"); ?>
<!--END #header-->

<div class="mainPart">
<div id="area">
	<h2>第二步</h2>
	<div id="identity">
		<h3>身份信息</h3>
		<ul>
			<li>学号：<span><?php echo $r['register']['num'] ?></span>（为今后登陆账号）</li>
			<li>姓名：<span><?php echo $r['register']['name']; ?></span></li>
			<li>性别：<span><?php echo $r['register']["sex"]==1?"男":"女" ?></span></li>
			<li>学院：<span><?php echo $r['register']['academy']; ?></span></li>
			<li>专业：<span><?php echo $r['register']['major']; ?></span></li>
			<li>生日：<span><?php echo $r['register']['birthday']; ?></span></li>
		</ul>
	</div>
	<form  method="post" name="send" action="<?php e_action("regSubmit"); ?>">
	<div id="information">
		<h3>信息完善</h3>
		<h4>账号信息</h4>
		<ul>
			<input type="hidden" name="username" value="<?php echo $r['register']['name'] ?>" />
			<input type="hidden" name="num" value="<?php echo $r['register']['num'] ?>" />
			<input type="hidden" name="IDnum" value="<?php echo $r['register']['IDnum'] ?>" />
			<li><label for="password"><span class="star">*</span>密 &nbsp; 码</label>
				<input type="password" name="password" id="password" data-validate="password" data-describedby="passwordMsg" data-description="password"/><div class="massages" ><div id="passwordMsg"></div></div></li>
			<li><label for="password_again"><span class="star">*</span>确认密码</label>
				<input type="password" name="password2" id="password_again" data-validate="password_again" data-describedby="password_againMsg" data-description="password_again"/><div class="massages" ><div id="password_againMsg"> </div></div>
			</li>
			<li><label for="weixin"><span class="star">*</span>微信账号</label>
			<input type="text" name="wechat" id="weixin" data-validate="required" data-describedby="weixinMsg" data-description="weixin"/><div class="massages" ><div id="weixinMsg"> </div></li>
		</ul>
		
		
		<h4>个人信息</h4>
		<ul>
			<li><label for="phone_num"><span class="star">*</span>手机号码</label>
				<input type="tel" name="tel" id="phone_num" data-validate="phone" data-describedby="phone_numMsg" data-description="phone_num"/><div class="massages" ><div id="phone_numMsg"></div></div></li>
			<li><label for="team">所属机构</label>
				<input type="text" name="affiliations" id="house" data-validate="required" data-describedby="houseMsg" data-description="house"/><div class="massages" ><div id="houseMsg"></div></div></li>
			<li><label for="QQ">QQ</label>
				<input type="text" name="qq" id="QQ" data-validate="required" data-describedby="QQMsg" data-description="QQ"/><div class="massages"><div id="QQMsg"></div></div></li>
			<li><label for="hobby">爱好特长</label>
				<input type="text" name="love" id="hobby" data-validate="required" data-describedby="hobbyMsg" data-description="hobby"/><div class="massages"><div id="hobbyMsg"></div></div></li>
			<li><label for="personal_text">个人说明</label>
				<textarea name="personal_text" id="personal_text" data-validate="required" data-describedby="personal_textMsg" data-description="personal_text"></textarea><div class="massages"><div id="personal_textMsg"></div></div></li>
		</ul>
		</div>
	</div>
	<div class="clear"></div>
	<div id="subRgt"><input type="submit" value="注册" class="btn"/></div>
	</form>
</div>
<div class="clear"></div>
</div>

<footer>
<!-- BEGIN #tail -->
	<?php import_part("Custom.module","tail"); ?>
<!-- END #tail -->
</footer>

<!-- [if lt IE 9]><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif] -->

</body>
</html>