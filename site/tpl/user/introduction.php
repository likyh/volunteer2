<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>志愿服务网-个人主页</title>
<base href="<?php echo $system['siteRoot'];?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

　<!--[if lt IE 9]>
　　　　<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
　　<![endif]-->
<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/personal.css"/>
<link rel="stylesheet" type="text/css" href="style/register.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
</head>

<body>
	<?php import_part("Custom.module","uTopNav"); ?>
		<div id="myHome">		
	<?php import_part("Custom.module","uSideNav"); ?>
			
			<div class="myHome_content">

		<!-- BEGIN使用说明 -->
				<div class="locateNav">
					<span>当前位置:</span>
					<a href="<?php e_page("user", "introduction") ?>">使用说明</a>
				</div>
					<div class="contentField">
					<h3>使用说明</h3>
						<div class="myAct_content">
							<img src="style/introduce.jpg">
						</div>
					</div>
				</div>
		<!-- END个人主页 -->

	</div>
</html>
</body>
</html>
