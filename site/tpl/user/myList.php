<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>志愿服务网-个人主页</title>
<base href="<?php echo $system['siteRoot'];?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/personal.css"/>
<link rel="stylesheet" type="text/css" href="style/register.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
</head>

<body>
	<?php import_part("Custom.module","uTopNav"); ?>
		<div id="myHome">		
	<?php import_part("Custom.module","uSideNav"); ?>
			
			<div class="myHome_content">

			<!--BEGIN我报名的活动名称-->
			<div class="locateNav">
				<span>当前位置:</span>
				<a href="<?php e_page("user", "userIndex") ?>">个人首页</a>
				>
				<a href="<?php e_page("user", "myList") ?>">我报名的活动</a>
			</div>
			
				<div class="contentField">
					<h3>我报名的活动</h3>
					<div class="myAct_content">
						<table>
							<tr>
								<th>活动名称（↓点击名称查看详情）</th>
								<th>更新时间</th>	
								<th>审核通过时长</th>
							</tr><?php foreach($r['list'] as $key=>$value) {?>
							<tr>
								<td class="myAvt_name"><a href="<?php e_page("user", "myContent",array('id'=>$value['activity_id'])) ?>"><?php echo $value['name']?></a></td> 
								<td class="passtime"><?php echo $value['audit_time']?></td>
								<td class="passtime"><span id="passTime"><?php echo $value['totalLength'] ?></span>分钟</td>
							</tr><?php }?>
						</table>
					</div>
					<div class="TotalTime">
					通过总时长<span class="passTotalTime"><?php echo $r['activitySum'] ?></span>分钟
					</div>
			</div>
		<!--END我报名的活动名称-->

	</div>
　<!--[if lt IE 9]>
　　　　<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
　<![endif]-->
</body>
</html>
