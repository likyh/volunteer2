<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>志愿服务网-个人主页</title>
<base href="<?php echo $system['siteRoot'];?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
　<!--[if lt IE 9]>
　　　　<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
　　<![endif]-->
<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/personal.css"/>
<link rel="stylesheet" type="text/css" href="style/register.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
</head>

<body>
	<?php import_part("Custom.module","uTopNav"); ?>
		<div id="myHome">		
	<?php import_part("Custom.module","uSideNav"); ?>
			
			<div class="myHome_content">

			<!--BEGIN 密码修改-->
				<div class="locateNav">
					<span>当前位置:</span>
					<a href="<?php e_page("user", "userIndex") ?>">个人首页</a>
					>
					<a href="<?php e_page("user", "alterPass") ?>">密码修改</a>
				</div>
				
					<div class="contentField">
						<h3>密码修改</h3>
						<div class="myAct_content">
							<div id="code">
								<ul>
									<form  method="post" name="send" onSubmit="return Check()" action="">
										<li><label for="old_password">原密码</label><input type="password" name="password" id="old_password" placeholder="输入原设密码"/></li>
										<li><label for="new_password1">新密码</label><input type="password" name="new_password1" id="new_password1" placeholder="输入新密码"/></li>
										<li><label for="new_password2">再次输入</label><input type="password" name="new_password2" id="new_password2" placeholder="请再次输入相同的新密码"/></li>
										<input type="submit" value="提交" name="提交" class="btn"/>
									</form>
								</ul>
							</div>
						</div>
						
				</div>
			<!--END 密码修改-->

	</div>
</html>
</body>
</html>
