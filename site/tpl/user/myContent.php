<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>志愿服务网-个人主页</title>
<base href="<?php echo $system['siteRoot'];?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/personal.css"/>
<link rel="stylesheet" type="text/css" href="style/register.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
</head>

<body>
	<?php import_part("Custom.module","uTopNav"); ?>
		<div id="myHome">		
	<?php import_part("Custom.module","uSideNav"); ?>
			
			<div class="myHome_content">

		<!--BEGIN我报名的活动详情-->
			<div class="locateNav">
				<span>当前位置:</span>
				<a href="<?php e_page("user", "userIndex") ?>">个人首页</a>
				>
				<a href="<?php e_page("user", "myList") ?>'">我报名的活动</a>
				>
				活动详情
			</div>
			<div class="contentField">
					<h3>我报名的活动</h3>
					<div class="myAct_content">
						<div class="back"><a href="<?php e_page("user", "myList") ?>">返回</a></div>
						<h4><?php echo $r['activity']['name']?></h4>
						<table>
							<tr>
								<td class="hoster">主办方</td>
								<td class="hoste_name"><?php echo $r['activity']['organizer']?></td>
								<td class="empty" colspan="2">&nbsp;</td>
								<td class="act_state">活动状态</td>
								<td class="pass_Y"><?php echo $r['activity']['state']?></td>
							</tr>
						</table>
						<table>
							<tr>
								<th colspan="6">活动审核情况</th>
							</tr>
							<tr>
								<td class="myAvt_name">我报名的活动时间</td>
								<td class="myAvt_name">审核方</td>
								<td class="myAvt_name">审核状态</td>
								<td class="passTime">审核时间</td>
								<td class="passtime">审核通过时长</td>
								<td class="cancel">删除（取消报名）</td>
							</tr>
							<?php foreach($r['time'] as $key=>$value){ ?>
							<tr>
								<td class="pass_time"><?php //echo $value['date']?>   <?php echo $value['start'] ?>~<?php echo $value['end']?></td>
								<td class="myAvt_name"><?php echo $r['activity']['author']?></td>
								<td class="<?php
										if($value['rejected']=='0') {
											if($value['checked']=='1'){
												echo "pass_Y";
											}else { echo "pass_U";}											
										} else { echo "pass_N";}
									?>"><?php
									if($value['rejected']=='0') {
										if($value['checked']=='1'){
											echo "已通过";
										}else { echo "待审核";}	
									} else { echo "未通过";}
									?></td>
								<td class="passTime"><?php echo $value['audit_time'] ?></td>
								<td class="passtime"><?php  if($value['checked']==1)  {echo $value['length'];} else {echo "0" ;}?>分钟</td>
								<?php if(($value['expired']=='0')&&($value['checked']=='0')&&($value['state_id']!='3')){ ?>
								<td class="cancel"><a target="_blank" href="<?php e_page("user", "delete",array('id'=>$value['timeid'])); ?>">删除</a></td><?php }else{?> 
								<td class="cancel">- -</td>
							</tr><?php } }?>
						</table>
						<div class="attention"><p>注：请慎重删除已报名的活动，活动招募暂停和招募结束时不可取消报名。</p></div>
						<div class="TotalTime">
							本活动通过总时长为<span class="passTotalTime"><?php echo $r['timeSum']  ?></span>分钟
						</div>
						<div id="part_more">
							<a href="<?php e_page("act", "content",array('id'=>$r['activity']['id'])) ?>"><input type="button" value="继续参与更多" class="btn"></a>
						</div>
					</div>
					
			</div>
		<!--END我报名的活动详情-->
	</div>
　<!--[if lt IE 9]>
　　　　<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
　　<![endif]-->
</body>
</html>
