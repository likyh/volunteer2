<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>志愿服务网-个人主页</title>
<base href="<?php echo $system['siteRoot'];?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
　<!--[if lt IE 9]>
　　　　<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
　　<![endif]-->
<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/personal.css"/>
<link rel="stylesheet" type="text/css" href="style/register.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
</head>

<body>
	<?php import_part("Custom.module","uTopNav"); ?>
		<div id="myHome">		
	<?php import_part("Custom.module","uSideNav"); ?>
			
			<div class="myHome_content">

		<!----------BEGIN 个人信息修改------------>
				<div class="locateNav">
					<span>当前位置:</span>
					<a href="<?php e_page("user", "userIndex") ?>">个人首页</a>
					>
					<a href="<?php e_action("alterInfo") ?>">个人信息修改</a>
				</div>
				
					<div class="contentField">
						<h3>个人信息修改</h3>
						<div class="myAct_content">
							<div id="identity">
							<h4>身份信息</h4>
							<ul>
								<li>学号：<span><?php echo $r['user_info']['num']?></span>（为登陆账号）</li>
								<li>姓名：<span><?php echo $r['user_info']['name']?></span></li>
								<li>性别：<span><?php echo $r['user_info']['sex']=='1' ? '男' : '女'?></span></li>
								<li>学院：<span><?php echo $r['user_info']['academy']?></span></li>
								<li>行政班级：<span><?php echo $r['user_info']['class']?></span></li>
								<li>所属机构：<span><?php echo $r['user_info']['affiliations']?></span></li>
								<li>电话：<span><?php echo $r['user_info']['tel']?></span></li>
								<li>QQ：<span><?php echo $r['user_info']['qq']?></span></li>
							</ul>
							</div>
							<div id="information">
							<h4>详细信息修改</h4>
							<ul>
								<form  method="post" name="send" onSubmit="return Check()">
									<li><label for="team">所属机构</label><input type="text" name="team" id="team"/></li>
									<li><label for="userNum1">手机号码</label><input type="text" name="tel" id="userNum1" placeholder="请输入11位手机号码"/></li>
									<li><label for="userNum2">QQ</label><input type="text" name="qq" id="userNum2"/></li>
									<input type="submit" value="提交" name="submit" class="btn"/>
								</form>
							</ul>
							</div>
						</div>
					<div class="clear"></div>
				</div>
			<!----------END 个人信息修改------------>

	</div>
</html>
</body>
</html>
