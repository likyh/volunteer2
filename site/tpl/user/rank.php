<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>志愿服务网-个人主页</title>
<base href="<?php echo $system['siteRoot'];?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

<link rel="stylesheet" type="text/css" href="style/reset.css"/>
<link rel="stylesheet" type="text/css" href="style/personal.css"/>
<link rel="stylesheet" type="text/css" href="style/register.css"/>
<link rel="stylesheet" type="text/css" href="style/commom.css"/>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312"/>
</head>

<body>
	<?php import_part("Custom.module","uTopNav"); ?>
		<div id="myHome">		
	<?php import_part("Custom.module","uSideNav"); ?>
			
			<div class="myHome_content">

		<!-- BEGIN 活动排名 -->
			<div class="locateNav">
				<span>当前位置:</span>
				<a href="<?php e_action("userIndex") ?>l">个人首页</a>
				>
				<a href="<?php e_action("rank") ?>">服务时长排名</a>
			</div>
			
				<div class="contentField">
					<h3>服务时长排名</h3>
					<div class="myAct_content">
						<div class="TotalTime"><?php if(isset($r['userRank'])&&(!empty($r['userRank']))){?>
							<span class="passTotalTime"><?php echo $r['username']  ?></span>同学,您的服务时间总长为<span class="passTotalTime"><?php echo $r['sum']  ?></span>分钟，排名为第<span class="passTotalTime"><?php echo $r['userRank'] ?></span>名
			<?php }else {?>				<span class="passTotalTime"><?php echo $r['username']  ?></span>,您未参与过任何服务，目前还未进入排名。<?php }?>
						</div>
						
						<div class="myAct_content">
						<table>
							<tr>
								<th>活动时长排名</th>
								<th>志愿者用户</th>	
								<th>审核通过时长</th>
							</tr><?php foreach($r['rank'] as $key=>$value) {?>
							<tr>
								<td class="myAvt_name"><?php echo $value['rank'] ?></td>
								<td class="passtime"><?php echo $value['name'] ?></td>
								<td class="passtime"><span id="passTime"><?php echo $value['sum'] ?></span>分钟</td>
							</tr><?php }?> 
						</table>
					</div>
						
					</div>
				</div>
		<!----------END 活动排名------------>		

	</div>
　<!--[if lt IE 9]>
　　　　<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
　　<![endif]-->
</body>
</html>
		