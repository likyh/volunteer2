<?php if(!$result['total']){?>
            暂无新闻
<?php }else{ ?>
<div id="data">
    <table id="dataTable" >
        <thead>
        <tr>
            <th>id</th>
            <th>标题</th>
            <th>作者</th>
            <th>时间</th>
            <th>修改/删除</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>id</th>
            <th>标题</th>
            <th>作者</th>
            <th>时间</th>
            <th>修改/删除</th>
        </tr>
        </tfoot>
        <tbody>
        <?php foreach($result['news'] as $k=>$v){?>
        <tr>
            <td><?php echo $v['id'] ?></td>
            <td><?php echo $v['title'] ?></td>
            <td><?php echo $v['author'] ?></td>
            <td><?php echo $v['date'] ?></td>
            <th><a href="<?php e_page("news", "modify",array('id'=>$v['id'])); ?>">修改</a> /
                <a href="<?php e_page("news", "delete",array('id'=>$v['id'])); ?> " onclick="if(!confirm('确认删除?')) return false;" >删除</a> </th>
        </tr>
        <?php }?>
        </tbody>
    </table>
    <?php
    /** @var Page $page */
    $page=$result['page'];
    ?>
    <?php echo $page->getPageHtml();?>
</div>
<?php }?>