<form action="<?php e_action("createSubmit"); ?>" method="post">
    <fieldset>
<!--        <legend>表单组1</legend>-->
        <input type="hidden" name="id" />
        <label for="title">新闻标题</label>
        <input type="text" name="title" id="title" placeholder="请输入新闻标题"/>
        <br/>
        <label for="author">作者</label>
        <input type="text" name="author" id="author" placeholder="请输入作者"/>
        <br/>
        <br/>
        <label for="date">日期</label>
        <input type="date" name="date" id="date" placeholder="格式 2015-01-01"/>
        <br/>
        <label for="typeid">发布者</label>
        <input type="text" name="typeid" id="typeid" readonly="readonly" value="<?php echo $result['author'];?>"/>
        <br/>
    </fieldset>

    <fieldset>
<!--        <legend>文本</legend>-->
        <label for="contentInput">内容</label>
        <script id="contentInput" name="content" class="editor" type="text/plain"></script>
    </fieldset>
    <input type="submit" value="添加"/>
</form>