<form action="<?php e_page("group", "modifySubmit"); ?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend></legend>
        <input type="hidden" name="id"  value="<?php echo $result['id']?>"/>
        <label for="title">团体名称</label>
        <input type="text" name="title" id="title" placeholder="请输入团体名称" value="<?php echo $result['title']?>"/>
    </fieldset>

    <fieldset>
        <legend></legend>
        <label for="contentInput">主要内容</label>
        <script id="contentInput" name="content" class="editor" type="text/plain"><?php echo $result['content'] ?></script>
    </fieldset>
    <input type="submit" value="保存修改"/>
</form>