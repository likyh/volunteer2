<?php if(!$result['total']){?>
    暂无公益团体
    <br/>
    <br/>
    <br/>
<?php }else{ ?>
<div id="data">
    <table id="dataTable" >
        <thead>
        <tr>
            <th>id</th>
            <th>名称</th>
            <?php if($result['type_id']==1||$result['type_id']==3){?>
                <th>修改/删除</th>
            <?php } ?>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>id</th>
            <th>名称</th>
            <?php if($result['type_id']==1||$result['type_id']==3){?>
                <th>修改/删除</th>
            <?php } ?>
        </tr>
        </tfoot>
        <tbody>
        <?php foreach($result['group'] as $k=>$v){?>
        <tr>
            <td><?php echo $v['id'] ?></td>
            <td><?php echo $v['title'] ?></td>
            <?php if($result['type_id']==1||$result['type_id']==3){?>
            <th><a href="<?php e_page("group", "modify",array('id'=>$v['id'])); ?>">修改</a> /
                <a onclick="if(!confirm('确认删除?')) return false;" href="<?php e_page("group", "delete",array('id'=>$v['id'])); ?>">删除</a> </th>
            <?php } ?>
        </tr>
        <?php }?>
        </tbody>
    </table>
    <?php
    /** @var Page $page */
    $page=$result['page'];
    ?>
    <?php echo $page->getPageHtml();?>
</div>
<?php }?>