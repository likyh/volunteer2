<form action="<?php e_page("mien", "modifySubmit"); ?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend></legend>
        <input type="hidden" name="id"  value="<?php echo $result['id']?>"/>
        <label for="title">志愿者姓名</label>
        <input type="text" name="title" id="title" placeholder="请输入志愿者姓名" value="<?php echo $result['title']?>"/>
        <br/>
        <label for="picurl">志愿者图片</label>
        <img src="<?php echo $result['picurl']?>" height="350px">
        <input type="file" name="picurl" id="picurl"/>
        (重新上传将替换原图)
        <br/>
    </fieldset>
    <input type="submit" value="保存修改"/>
</form>