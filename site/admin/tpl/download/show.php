<?php if(!$result['total']){?>
    暂无下载资料
    <br/>
    <br/>
    <br/>
<?php }else{ ?>
<div id="data">
    <table id="dataTable" >
        <thead>
        <tr>
            <th>id</th>
            <th>姓名</th>
            <th>时间</th>
            <?php if($result['type_id']==1||$result['type_id']==3){?>
                <th>修改/删除</th>
            <?php } ?>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>id</th>
            <th>姓名</th>
            <th>时间</th>
            <?php if($result['type_id']==1||$result['type_id']==3){?>
                <th>修改/删除</th>
            <?php } ?>
        </tr>
        </tfoot>
        <tbody>
        <?php foreach($result['download'] as $k=>$v){?>
        <tr>
            <td><?php echo $v['id'] ?></td>
            <td><?php echo $v['title'] ?></td>
            <td><?php echo $v['create_time'] ?></td>
            <?php if($result['type_id']==1||$result['type_id']==3){?>
            <th><a href="<?php e_page("download", "modify",array('id'=>$v['id'])); ?>">修改</a> /
                <a href="<?php e_page("download", "delete",array('id'=>$v['id'])); ?>" onclick="if(!confirm('确认删除?')) return false;">删除</a> </th>
            <?php } ?>
        </tr>
        <?php }?>
        </tbody>
    </table>
    <?php
    /** @var Page $page */
    $page=$result['page'];
    ?>
    <?php echo $page->getPageHtml();?>
</div>
<?php }?>