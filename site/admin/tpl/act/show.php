<div id="data">
    <table id="dataTable" >
        <thead>
        <tr>
            <th>id</th>
            <th>标题</th>
            <th>地点</th>
            <th>组织者</th>
            <th>发布者</th>
            <th>活动状态</th>
            <th>时间</th>
            <th>具体时间</th>
            <?php if($result['level']){?>
                <th>审核</th>
            <?php } ?>
            <th>修改/删除</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>id</th>
            <th>标题</th>
            <th>地点</th>
            <th>组织者</th>
            <th>发布者</th>
            <th>活动状态</th>
            <th>时间</th>
            <th>具体时间</th>
            <?php if($result['level']){?>
            <th>审核</th>
            <?php } ?>
            <th>修改/删除</th>
        </tr>
        </tfoot>
        <tbody>
        <?php foreach($result['act'] as $k=>$v){?>
        <tr>
            <td><?php echo $v['id'] ?></td>
            <td><?php echo $v['name'] ?></td>
            <td><?php echo $v['place'] ?></td>
            <td><?php echo $v['organizer'] ?></td>
            <td><?php echo $v['author'] ?></td>
            <td><?php echo $v['state'] ?></td>
<!--            <td>--><?php //echo $v['time'] ?><!--<br/>-->
<!--           <a href="--><?php //e_page("act", "timeShow",array('id'=>$v['id'])); ?><!--">具体活动时间</a></td>-->
            <td><?php echo $v['time'] ?></td>
            <td><a href="<?php e_page("act", "selectTime",array('id'=>$v['id'])); ?>">具体时间</a></td>
            <?php if($result['level']){?>
            <td><?php if($v['auth']==1){echo '已审核';}else{?>
                <a href="<?php e_page("act", "auth",array('id'=>$v['id'])); ?>">审核</a>
                <?php } ?></td>
            <?php } ?>
            <th><a href="<?php e_page("act", "modify",array('id'=>$v['id'])); ?>">修改</a> /
                <a onclick="if(!confirm('确认删除?')) return false;" href="<?php e_page("act", "delete",array('id'=>$v['id'])); ?>">删除</a> </th>
        </tr>
        <?php }?>
        </tbody>
    </table>
    <?php
    /** @var Page $page */
    $page=$result['page'];
    ?>
    <?php echo $page->getPageHtml();?>
</div>