<?php if(!$result['total']){?>
    暂无报名者
    <br/>
    <br/>
    <br/>
    <a href="<?php e_page("act", "userAdd",array('time_id'=>$result['time_id'])); ?>">添加报名者</a>
<?php }else{ ?>
<div id="data">
    <table id="dataTable" >
        <thead>
        <tr>
            <th>id</th>
            <th>学号</th>
            <th>姓名</th>
            <th>电话</th>
            <th>日期</th>
            <th>申请时长</th>
            <th>当前状态</th>
            <th>审核<?php if($result['level']){?>/否决<?php }?></th>
            <th>删除</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>id</th>
            <th>学号</th>
            <th>姓名</th>
            <th>电话</th>
            <th>日期</th>
            <th>申请时长</th>
            <th>当前状态</th>
            <th>审核<?php if($result['level']){?>/否决<?php }?></th>
            <th>删除</th>
        </tr>
        </tfoot>
        <tbody>
        <?php foreach($result['user'] as $k=>$v){?>
        <tr>
            <td><?php echo $v['id'] ?></td>
            <td><?php echo $v['num'] ?></td>
            <td><?php echo $v['name'] ?></td>
            <td><?php echo $v['tel'] ?></td>
            <td><?php echo date('Y-m-d',strtotime($v['start'])) ?></td>
            <td><?php echo $v['length'] ?></td>
            <td><?php 
            if($v['checked']){ echo '已审核';}elseif ($v['rejected']) {
            echo '已否决';}else{ echo '暂无';} ?>
            </td>
            <td><?php if($v['checked']||$v['rejected']){echo '- -';}else{?>
                <a href="<?php e_page("act", "userAuth",array('id'=>$v['id'],'time_id'=>$result['time_id'])); ?>">审核</a><?php }?><?php if($result['level']){?>/
                <?php if($v['rejected']||$v['checked']){echo '- -';}else{?><a href="<?php e_page("act", "reject",array('id'=>$v['id'],'time_id'=>$result['time_id'])); ?>">否决</a><?php } ?><?php }?>
            </td>
            <th><a href="<?php e_page("act", "userDelete",array('id'=>$v['id'],'time_id'=>$result['time_id'])); ?>" onclick="if(!confirm('确认删除?')) return false;">删除</a> </th>
        </tr>
        <?php }?>
        </tbody>
    </table>
    <br/>
    <a href="<?php e_page("act", "userAdd",array('time_id'=>$result['time_id'])); ?>">添加报名者</a>
    <?php
    /** @var Page $page */
    $page=$result['page'];
    ?>
    <?php echo $page->getPageHtml();?>
</div>
<?php }?>