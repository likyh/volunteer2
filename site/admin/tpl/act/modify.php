<form action="<?php e_page("act", "modifySubmit"); ?>" method="post" enctype="multipart/form-data">
    <fieldset>
<!--        <legend>表单组1</legend>-->
        <input type="hidden" name="id" value="<?php echo $result['id'] ?>"/>
        <label for="name">活动标题</label>
        <input type="text" name="name" id="title" placeholder="请输入活动名称" value="<?php echo $result['name'] ?>"/>
        <br/>
        <label for="select1">活动状态</label>
        <select name="state_id" id="select1">
            <?php foreach ($result['state'] as $v) {?>
                <option value="<?php echo $v['id'] ?>" <?php if($v['id']==$result['state_id']) echo 'selected' ?>><?php echo $v['name'] ?></option>
            <?php }?>
        </select>
        <br/>
        <br/>
        <label for="nature">活动性质</label>
        <select name="nature" id="nature">
            <?php if($result['nature']=="常规活动"){?>
            <option value="常规活动" <?php echo 'seleccted'?>>常规活动</option>
            <option value="非常规活动">非常规活动</option>
            <?php }else{?>
                <option value="非常规活动" <?php echo 'seleccted'?>>非常规活动</option>
                <option value="常规活动">常规活动</option>
            <?php } ?>
        </select>
        <br/>
        <label for="author">发布者</label>
        <input type="text" name="author" id="author" readonly="readonly" value="<?php echo $result['author'] ?>"/>
        <br/>
        <label for="organizer">组织者</label>
        <input type="text" name="organizer" id="organizer" value="<?php echo $result['organizer'] ?>"/>
        <br/>
        <label for="time">活动时间</label>
        <input type="text" name="time" id="time" value="<?php echo $result['time'] ?>"/>
        <br/>
        <label for="place">活动地点</label>
        <input type="text" name="place" id="place" value="<?php echo $result['place'] ?>"/>
        <br/>
        <label for="picurl">活动图片</label>
        <img src="<?php echo $result['picurl'] ?>" />
        <input type="file" name="picurl" id="picurl"/><br/>
        (重新上传将替换原来的图)
        <br/>
    </fieldset>

    <fieldset>
        <legend>文本</legend>
        <label for="description">简介</label>
        <textarea name="description" id="description" ><?php echo $result['description'] ?></textarea>

        <label for="contentInput">内容</label>
        <script id="contentInput" name="content" class="editor" type="text/plain"><?php echo $result['content'] ?></script>
    </fieldset>
    <input type="submit" value="保存修改"/>
</form>