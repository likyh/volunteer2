<form action="<?php e_action("createSubmit"); ?>" method="post"  enctype="multipart/form-data">
    <fieldset>
<!--        <legend>表单组1</legend>-->
        <input type="hidden" name="id" />
        <label for="name">活动名称</label>
        <input type="text" name="name" id="title" placeholder="请输入活动名称"/>
        <br/>
        <label for="select1">活动状态</label>
        <select name="state_id" id="select1">
            <?php foreach ($result['state'] as $v) {?>
                <option value="<?php echo $v['id'] ?>"><?php echo $v['name'] ?></option>
            <?php }?>
        </select>
        <br/>
        <br/>
        <label for="nature">活动性质</label>
        <select name="nature" id="nature">
            <option value="0">常规活动</option>
            <option value="1">非常规活动</option>
        </select>
        <br/>
        <label for="author">发布者</label>
        <input type="text" name="author" id="author" readonly="readonly" value="<?php echo $result['author'] ?>"/>
        <br/>
        <label for="organizer">组织者</label>
        <input type="text" name="organizer" id="organizer" placeholder="请输入组织者"/>
        <br/>
        <label for="time">活动时间</label>
        <input type="text" name="time" id="time" placeholder="请输入活动时间"/>
        <br/>
        <label for="place">活动地点</label>
        <input type="text" name="place" id="place" placeholder="请输入活动地点"/>
        <br/>
        <label for="picurl">活动图片</label>
        <input type="file" name="picurl" id="picurl"/>
        <br/>
    </fieldset>

    <fieldset>
<!--        <legend>文本</legend>-->
        <label for="description">活动简介</label>
        <textarea name="description" id="description"></textarea>

        <label for="contentInput">活动内容</label>
        <script id="contentInput" name="content" class="editor" type="text/plain"></script>
    </fieldset>
    <input type="submit" value="添加"/>
</form>