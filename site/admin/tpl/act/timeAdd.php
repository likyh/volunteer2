<form action="<?php e_action("timeAddSubmit"); ?>" method="post"  enctype="multipart/form-data">
    <fieldset>
        <legend></legend>
        <input type="hidden" name="act_id" value="<?php echo $result['act_id']; ?>"/>
        <label for="date">活动日期</label>
        <input type="date" name="date" id="date" placeholder="格式  2015-01-01"/>
        <br/>
        <label for="startInput">开始时间</label>
        <input type="text" name="startTime" id="startInput" placeholder="格式  01:00:00"/>
        <br/>
        <label for="endInput">结束时间</label>
        <input type="text" name="endTime" id="endInput" placeholder="请输入活动结束时间"/>
        <br/>
        <label for="length">活动时长</label>
        <input type="text" name="length" id="length" placeholder="请输入活动时长（分钟）"/>
        <br/>
        <label for="total_num">活动人数</label>
        <input type="text" name="total_num" id="total_num" placeholder="请输入活动人数"/>
        <br/>
    </fieldset>
    <input type="submit" value="添加"/>
</form>