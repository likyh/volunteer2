<?php if(!$result['total']){?>
    暂无具体时间
    <br/>
    <br/>
    <br/>
    <a href="<?php e_page("act", "timeAdd",array('act_id'=>$result['act_id'])); ?>">添加时间</a>
<?php }else{ ?>
<div id="data">
    <table id="dataTable" >
        <thead>
        <tr>
            <th>id</th>
            <th>活动日期</th>
            <th>开始时间</th>
            <th>结束时间</th>
            <th>活动时长（分钟）</th>
            <th>人数</th>
            <th>报名用户</th>
            <th>是否过期</th>
            <th>删除</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>id</th>
            <th>活动日期</th>
            <th>开始时间</th>
            <th>结束时间</th>
            <th>活动时长（分钟）</th>
            <th>人数</th>
            <th>报名用户</th>
            <th>是否过期</th>
            <th>删除</th>
        </tr>
        </tfoot>
        <tbody>
        <?php foreach($result['time'] as $k=>$v){?>
        <tr>
            <td><?php echo $v['id'] ?></td>
            <td><?php echo date('Y-m-d',strtotime($v['start'])) ?></td>
            <td><?php echo date('H:i:s',strtotime($v['start'])) ?></td>
            <td><?php echo date('H:i:s',strtotime($v['end'])) ?></td>
            <td><?php echo $v['length'] ?></td>
            <td><?php echo $v['existing_num'].'/'.$v['total_num'] ?></td>
            <td><a href="<?php e_page("act", "selectAct",array('id'=>$v['id'])); ?>">查看报名者信息</a></td>
            <td><?php if($v['expired']) {echo '已过期';}else{echo '未过期';} ?></td>
<!--            <th><a onclick="return makeSureDelete();" href="--><?php //e_page("act", "timeDelete",array('id'=>$v['id'])); ?><!--">删除</a> </th>-->
            <th><a href="<?php e_page("act", "timeDelete",array('id'=>$v['id']));?>" onclick="if(!confirm('确认删除?')) return false;">删除</a> </th>
        </tr>
        <?php }?>
        </tbody>
    </table>
    <br/>
    <a href="<?php e_page("act", "timeAdd",array('act_id'=>$result['act_id'])); ?>">添加时间</a>
    <?php
    /** @var Page $page */
    $page=$result['page'];
    ?>
    <?php echo $page->getPageHtml();?>
</div>
<?php }?>