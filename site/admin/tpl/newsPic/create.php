<form action="<?php e_action("createSubmit"); ?>" method="post"  enctype="multipart/form-data">
    <fieldset>
<!--        <legend>添加图片新闻</legend>-->
        <input type="hidden" name="id" />
        <label for="title">新闻标题</label>
        <input type="text" name="title" id="title" placeholder="请输入新闻标题"/>
        <br/>
        <label for="author">作者</label>
        <input type="text" name="author" id="author" placeholder="请输入作者"/>
        <br/>
        <br/>
        <label for="date">日期</label>
        <input type="date" name="date" id="date" placeholder="格式：2015-01-01"/>
        <br/>
        <br/>
        <label for="typeid">发布者</label>
        <input type="text" name="typeid" id="typeid" readonly="readonly" value="<?php echo $result['author'];?>"/>
        <br/>
        <br/>
        <label for="picurl">首页图片</label>
        <input type="file" name="picurl" id="picurl"/>
        <br/>
        <br/>
    </fieldset>

    <fieldset>
<!--        <legend>文本</legend>-->
        <label for="contentInput">内容</label>
        <script id="contentInput" name="content" class="editor" type="text/plain"></script>
    </fieldset>
    <input type="submit" value="添加"/>
</form>