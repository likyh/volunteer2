<form action="<?php e_page("newsPic", "modifySubmit"); ?>" method="post" enctype="multipart/form-data">
    <fieldset>
<!--        <legend>表单组1</legend>-->
        <input type="hidden" name="id" value="<?php echo $result['id'] ?>" />
        <label for="title">新闻标题</label>
        <input type="text" name="title" id="title" placeholder="请输入新闻标题" value="<?php echo $result['title'] ?>"/>
        <br/>
        <label for="author">作者</label>
        <input type="text" name="author" id="author" value="<?php echo $result['author'] ?>"/>
    <br/>
        <label for="date">日期</label>
        <input type="text" name="date" id="date" value="<?php echo $result['date'] ?>"/>
    <br/>
        <label for="typeid">发布者</label>
        <input type="text" name="typeid" id="typeid" readonly="readonly" value="<?php echo $result['type'] ?>"/>
    <br/>
    <label for="picurl">首页图片</label>
    <img src="<?php echo $result['picurl'] ?>" />
        <input type="file" name="picurl" id="picurl" />
        (重新上传将替换原来的图)
        <br/>
    </fieldset>
    <br/>
    <fieldset>
        <legend>内容</legend>
        <script id="contentInput" name="content" class="editor" type="text/plain"><?php echo $result['content'] ?></script>
    </fieldset>
    <input type="submit" value="保存修改"/>
</form>