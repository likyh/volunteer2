<form action="<?php e_action("createSubmit"); ?>" method="post"  enctype="multipart/form-data">
    <fieldset>
<!--        <legend>表单组1</legend>-->
        <input type="hidden" name="id" />
        <label for="name">计划名称</label>
        <input type="text" name="name" id="name" placeholder="请输入计划名称"/>
        <br/>
        <label for="organizer">组织者</label>
        <input type="text" name="organizer" id="organizer" placeholder="请输入组织者"/>
        <br/>
        <br/>
        <label for="time">时间</label>
        <input type="date" name="time" id="time" placeholder="格式：2015-01-01"/>
        <br/>
        <label for="place">地点</label>
        <input type="text" name="place" id="place" placeholder="请输入地点"/>
        <br/>
        <br/>
        <label for="picurl">活动图片</label>
        <input type="file" name="picurl" id="picurl"/>
        <br/>
        <br/>
    </fieldset>

    <fieldset>
<!--        <legend>文本</legend>-->
        <label for="description">计划简介</label>
        <textarea name="description" id="description"></textarea>
        <label for="contentInput">主要内容</label>
        <script id="contentInput" name="content" class="editor" type="text/plain"></script>
    </fieldset>
    <input type="submit" value="添加"/>
</form>