<form action="<?php e_page("plan", "modifySubmit"); ?>" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend></legend>
        <input type="hidden" name="id"  value="<?php echo $result['id']?>"/>
        <label for="name">计划名称</label>
        <input type="text" name="name" id="name" placeholder="请输入计划名称" value="<?php echo $result['title']?>"/>
        <br/>
        <label for="organizer">组织者</label>
        <input type="text" name="organizer" id="organizer" value="<?php echo $result['organizer']?>"/>
        <br/>
        <label for="time">时间</label>
        <input type="text" name="time" id="time" value="<?php echo $result['time']?>"/>
        <br/>
        <label for="place">地点</label>
        <input type="text" name="place" id="place" value="<?php echo $result['place']?>"/>
        <br/>
        <label for="picurl">活动图片</label>
        <img src="<?php echo $result['picurl']?>">
        <input type="file" name="picurl" id="picurl"/><br/>
        (重新上传将替换原图)
        <br/>
    </fieldset>

    <fieldset>
        <legend></legend>
        <label for="description">专项计划简介</label>
        <textarea name="description" id="description"><?php echo $result['description']?></textarea>
        <label for="contentInput">主要内容</label>
        <script id="contentInput" name="content" class="editor" type="text/plain"><?php echo $result['content'] ?></script>
    </fieldset>
    <input type="submit" value="保存修改"/>
</form>